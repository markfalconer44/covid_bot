import os
import sys
import pathlib
import requests
import logging
import zipfile
import random
import traceback
import collections
import click
import re
import pandas as pd
from requests.exceptions import RequestException
from bs4 import BeautifulSoup
import bs4
from threading import Thread
from time import sleep
from pathlib import Path
from datetime import datetime, timedelta

top_dir = pathlib.Path(__file__).absolute().parent.parent.as_posix()
sys.path.append(top_dir)

from src import config
from src.response_codes import responses as resp_codes

# Enable logging
LOG_DIR = os.path.join(os.getcwd(), 'logs')
LOG_FILENAME = os.path.join(LOG_DIR, 'rotating_log')
os.makedirs(LOG_DIR, exist_ok=True)
logging.addLevelName(31, 'MESG')
logging.addLevelName(35, 'RECMESG')
LOGGING_LEVELS = [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL, 31, 35]
LOG_FORMATTER = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')


class Scraper:
    def __init__(self, log, bot_token, logger, bot, bot_files=os.path.join(os.getcwd(), 'bot_files')):
        self.should_log = log

        # Setup daily update
        self.check_for_data = True
        self.checker_thread = Thread(target=self.update_checker, daemon=False, args =(lambda : self.check_for_data, ))
        self.checker_thread.name = "Checker thread"

        self.bot_files_dir = bot_files
        self.bot = bot
        self.logger = logger

    def run(self):
        self.checker_thread.start()

    def simple_get(self, url):
        """
        Attempts to get the content at `url` by making an HTTP GET request.
        If the content-type of response is some kind of HTML/XML, return the
        text content, otherwise return None.

        [provided by https://dev.to/tiagomac/web-scraping-in-3-steps-with-python-2hnf]
        """
        try:
            resp = requests.get(url, stream=True)

            if self.is_good_response(resp):
                return resp.content, resp.status_code
            else:
                return None, resp.status_code

        except RequestException as e:
            self.logger.warning(
                msg='Error during requests to {0} : {1}'.format(url, str(e))
            )
            return None, None

    def is_good_response(self, resp):
        """
        Returns True if the response seems to be HTML, False otherwise.

        [provided by https://dev.to/tiagomac/web-scraping-in-3-steps-with-python-2hnf]
        """
        if resp.headers is None:
            return None

        content_type = resp.headers['Content-Type'].lower()
        return (resp.status_code == 200
                and content_type is not None
                and content_type.find('html') > -1)

    def get_scotland_info(self, raw_html):
        """ Extracts the daily data paragraph from the government website """
        if raw_html is None:
            return None

        html_string = str(raw_html, 'utf-8')
        # html_string = html_string.replace("> <", "><")

        # check opening and closing tags all exits (just checking number of opening matches
        # number of closing for elements i care about)
        open_tags = re.findall('<((?=\\b(h2|ul)\\b)[^/> !]+)([^>]*)>', html_string)
        close_tags = (re.findall('</((?=\\b(h2|ul)\\b)[^> !]+)([^>]*)>', html_string))
        paras = {}

        if len(open_tags) != len(close_tags):
            # uneven open and close tags
            return None

        html = BeautifulSoup(html_string, 'html.parser')
        result = html.find_all("div", {"class": config.scot_div})
        data = ""
        for res in result:

            # Get date
            temp = res.find("h2")
            # allow no date
            if temp is not None:
                data = temp.text + "\n\n"
            # find first unordered list and save it
            temp = res.find("ul")
            if temp is None or temp.text is "":
                return None

            # go through the contents of the bullet points
            for item in temp.contents:
                if not isinstance(item, bs4.Tag):
                    continue
                # handle sub bullet points
                for i in item.contents:
                    if not isinstance(i, bs4.Tag):
                        continue
                    nl = "\n"
                    if i.name == "ul":
                        data += f"{i.text.replace(nl, ' ')} "
                    else:
                        data += f"{i.text.rstrip()} "

                data += "\n"


            # get any additional paragraphs
            # if p object starts with a * then include it, make sure to only include each para twice
            paragraphs = res.find_all("p")
            for para in paragraphs:
                if para.text.startswith("*"):
                    asterik, text = re.findall("(\**)?(.*)", para.text)[0]
                    if asterik in data and asterik not in paras.keys() and asterik is not '':
                        # check asterik is used
                        paras[asterik] = f"{asterik} {text}"

        # add asterik text
        if len(paras) > 0:
            data += f"\n{config.para_split}"
        for p in paras:
            data += f"\n\n{paras[p]}"

        data = data.replace("\n", "\n\n")
        data = data.rstrip()  # gets rid of trailing newlines
        data = data.replace("  ", " ")  # get rid of double spaces

        # dont start with newlines
        while data.startswith("\n"):
            data = data[1:]
        return ''.join([i if ord(i) < 128 else ' ' for i in data])

    def get_comparison(self, cur_data, prev_day_data):

        deaths = None
        cases = None
        percent_newly_positive = None
        intensive_care = None
        hospital = None
        percent_positive_tests = None
        returned_tests = None
        first_vac = None
        second_vac = None
        third_vac = None

        try:
            deaths = int(cur_data["Deaths"])-int(prev_day_data["Deaths"])
        except:
            pass

        try:
            cases = int(cur_data["Number of cases"])-int(prev_day_data["Number of cases"])
        except:
            pass


        try:
            percent_newly_positive = round(float(cur_data['Percent Newly Positive'])-float(prev_day_data['Percent Newly Positive']),2)
        except:
            pass


        try:
            intensive_care = int(cur_data["People in Intensive Care"])-int(prev_day_data["People in Intensive Care"])
        except:
            pass


        try:
            hospital = int(cur_data["People in Hospital"])-int(prev_day_data["People in Hospital"])
        except:
            pass


        try:
            percent_positive_tests = round(float(cur_data["Percent Positive Tests"])-float(prev_day_data["Percent Positive Tests"]),2)
        except:
            pass


        try:
            returned_tests = int(cur_data["Returned Tests"])-int(prev_day_data["Returned Tests"])
        except:
            pass


        try:
            first_vac = int(cur_data["First Vaccination Dose"])-int(prev_day_data["First Vaccination Dose"])
        except:
            pass


        try:
            second_vac = int(cur_data["Second Vaccination Dose"])-int(prev_day_data["Second Vaccination Dose"])
        except:
            pass

        try:
            third_vac = int(cur_data["Third Vaccination Dose"])-int(prev_day_data["Third Vaccination Dose"])
        except:
            pass

        d={'Number of cases': cases, 'Percent Newly Positive': percent_newly_positive,
                        'Deaths': deaths, 'People in Intensive Care': intensive_care,
                        'People in Hospital': hospital, 'Returned Tests': returned_tests,
                        'Percent Positive Tests': percent_positive_tests,
            'First Vaccination Dose': first_vac, 'Second Vaccination Dose': second_vac,
            'Third Vaccination Dose': third_vac}

        return d

    # todo figure out how to mock while True loop
    def update_checker(self, check):
        """
        Loop that checks government site for updated information.
        Checks every 60 seconds.
        """
        self.logger.info("Update checker thread running ")
        errors_in_a_row = 0
        error_messages_sent = 1
        error_codes = []
        delay = 60
        try:
            while check():
                raw_html, status_code = self.simple_get(config.scot_url)
                new_data = self.get_scotland_info(raw_html)
                if new_data is None:
                    shrt_msg, msg = resp_codes[status_code]
                    if errors_in_a_row == 0:
                        self.logger.error(f"Received status code {status_code} from webpage. "
                                          f"Will ping you every 30 attempts where this fails.\n\n"
                                          f"Error code {status_code}: {msg}")

                    errors_in_a_row += 1
                    self.logger.warning(f"Data found was 'None'. Probably webpage is down. This has happened "
                                        f"{errors_in_a_row} times in a row. Sleeping for {delay} seconds. "
                                        f"Status code: {status_code}")

                    error_codes.append((status_code, shrt_msg))

                    lim = 10
                    if errors_in_a_row % (lim*((error_messages_sent)*(error_messages_sent))) == 0:
                        message = f"No data found {errors_in_a_row} times in a row.\n\nError codes:\n"
                        err_codes = collections.Counter(error_codes)
                        print(err_codes)
                        for d in sorted(err_codes):
                            code, info = d
                            occurrences = err_codes[d]
                            message += f"{occurrences} times: {code} - {info}\n"

                        message += f"\nLast error code: \n{status_code}: {msg}"

                        error_messages_sent += 1
                        self.logger.error(message)

                    sleep(delay)
                    continue

                data = None
                try:
                    extracted_new_data = self.extract_data(new_data)
                    with open(os.path.join(self.bot_files_dir, 'data.csv'), 'r') as csv_file:
                        lines = csv_file.readlines()
                        last_line = lines[-1].rstrip().strip()
                        old_data = tuple(last_line.split(","))


                    if isinstance(old_data, tuple) and isinstance(extracted_new_data, tuple):
                        valid = True
                    else:
                        valid = False

                    if extracted_new_data == old_data:
                        print("Data is the same")

                except FileNotFoundError as e:
                    # File doesn't exist, thats fine
                    self.logger.info('No previous info file')
                    valid = True
                    extracted_new_data = None

                if extracted_new_data is not None and extracted_new_data == old_data:
                    self.logger.debug(f'No change in data from old data to new data.\nOld data: {old_data}'
                                      f'\nNew data: {extracted_new_data}')

                elif valid:
                    data_parsed = new_data
                    data_formatted = ""
                    if "\n\n" in data_parsed:
                        data_parsed = new_data.replace("\n\n", "\n")
                    data_parsed = data_parsed.split("\n")
                    data_parsed[0] = data_parsed[0] + "\n\n"
                    data_nice = ""
                    for d in data_parsed:
                        if d == "":
                            continue
                        data_formatted += d + "\n\n"

                    if data is None:
                        data = "N/A"


                    prev_day_data = None
                    cur_data = self.log_gov_data(new_data)
                    try:
                        cur_date = cur_data["Date"]
                        cur_date_obj = datetime.strptime(cur_date, '%d %B %Y')
                        prev_date_obj = cur_date_obj-timedelta(1)

                        if "win" in sys.platform:
                            # windows
                            prev_date_string = prev_date_obj.strftime("%#d %B %Y")
                        else:
                            # unix
                            prev_date_string = prev_date_obj.strftime("%-d %B %Y")
                        self.logger.info(f"Checking for data on date {prev_date_string}.")
                        existing_data = pd.read_csv(os.path.join(self.bot_files_dir, 'data.csv'),header=None)
                        prev_data = existing_data[existing_data.iloc[:, 0] == prev_date_string].tail(1)
                        self.logger.info(prev_data)
                        self.logger.info(prev_data.iloc[0,0])
                        prev_day_data={'Date': prev_data.iloc[0,0], 'Number of cases': prev_data.iloc[0,1], 'Percent Newly Positive': prev_data.iloc[0,2],
                        'Deaths': prev_data.iloc[0,3], 'People in Intensive Care': prev_data.iloc[0,4],
                        'People in Hospital': prev_data.iloc[0,5], 'Returned Tests': prev_data.iloc[0,6],
                        'Percent Positive Tests': prev_data.iloc[0,7],
                        'First Vaccination Dose': prev_data.iloc[0,8], 'Second Vaccination Dose': prev_data.iloc[0,9],
                        'Third Vaccination Dose': prev_data.iloc[0,10]}
                    except Exception as e:
                        self.logger.error("ERROR OCCURRED IN RECEIVING PREV DAYS DATA. Error is: {} ({})".format(type(e), str(e.args)))
                        self.logger.warning(traceback.format_exc())

                    self.logger.info('Change in data from old data to new data.\n\tOld data: '
                        + data.replace("\n\n", "\n\t\t") + '\n\tNew data: '
                        + new_data.replace("\n\n", "\n\t\t"))
                    comparison = self.get_comparison(cur_data,prev_day_data)
                    self.daily_update("UPDATED DATA\n" + data_formatted, comparison)

                errors_in_a_row = 0
                error_messages_sent = 1
                error_codes = []
                # sleep for a minute no matter whether theres new data or not
                sleep(delay)

            self.logger.info("Infinite loop has exited.")
        except Exception as e:
            # Restarting thread if it crashes for any unknown reason
            self.logger.error(f"ERROR OCCURRED IN UPDATE LOOP. Error is: {type(e)} ({e.args})")
            sleep(10)
            self.checker_thread = Thread(target=self.update_checker, daemon=False, args =(lambda : self.check_for_data, ))
            self.checker_thread.start()
            for id in self.owners:
                self.send_text(
                    user_id=id,
                    text="Bot crashed. Thread restarted."
                )

    def extract_data(self, data):
        try:
            # split back into line by line
            if config.para_split in data:
                act_data = data.replace("\n\n", "\n").split(config.para_split)[0]
            else:
                act_data = data.replace("\n\n", "\n")
            act_data = act_data.split("\n")
            linebyline = tuple([val for val in act_data if val != ""])  # removes any empty lines or additional paragraphs

            # check correct number of lines recieved
            if len(linebyline) == 7:

                # expected phrases (in order of expected appearance)
                expected_words = ["Scottish numbers", "new cases", "new tests",
                                  "death", "intensive care", "hospital", "vaccination"]

                valid = True
                for i, line in enumerate(linebyline):
                    expected_phrase = expected_words[i]
                    if not expected_phrase in line:
                        self.logger.warning(
                            "Text received was in unexpected order. Text received "
                            + "was "
                            + data
                        )
                        valid = False

            else:
                valid = False
                self.logger.warning("Received incorrect number of lines from gov website. Text received " + "was " + data)
            # parse data into separate lines
            if valid:
                # unpack tuple
                date_line, case_line, tests_received_line, death_line, intensive_care_line, hospital_line, vacc_line = linebyline
            else:
                # set all strings to empty
                date_line, case_line, tests_received_line, death_line, intensive_care_line, hospital_line, vacc_line = (
                        [""] * 7)
                # assign lines that have been found
                for line in linebyline:
                    if "Scottish numbers" in line:
                        date_line = line
                    elif "new cases" in line:
                        case_line = line
                    elif "new tests" in line:
                        tests_received_line = line
                    elif "of these were positive" in line:
                        tests_positive_line = line
                    elif "death" in line:
                        death_line = line
                    elif "intensive care" in line:
                        intensive_care_line = line
                    elif "hospital" in line:
                        hospital_line = line
                    elif "vaccination" in line:
                        vacc_line = line

            """ Extract numbers from data"""

            # find todays date
            date_list = re.findall('^(?:.*[^\\d]: )(.+)', date_line)
            # has data been found?
            if len(date_list) > 0:
                date = date_list[0]
            else:
                date = "Error"

            # find number of cases and percentage
            cases_list = re.findall('^([\\d,]*)(?:(?:.* )(\\d*).(\\d*)%|(?:.* ))', case_line)  # ew
            # has data been found
            if len(cases_list) > 0:
                number_of_cases, percent_digit, percent_decimal = cases_list[0]
                number_of_cases = number_of_cases.replace(",", "")
                percent_cases = percent_digit + "." + percent_decimal

                # check valid result
                if number_of_cases is '':
                    number_of_cases = "Error"
                if percent_digit is '' or percent_decimal is '':
                    percent_cases = "No longer provided"
            else:
                number_of_cases = "Error"
                percent_cases = "Error"

            # find number of tests received
            tests_received_list = re.findall('^([\\d,]*)(?:(?:.* )(\\d*).(\\d*)%|(?:.* ))', tests_received_line)
            # has data been found?
            if len(tests_received_list) > 0:
                tests_received, percent_digit, percent_decimal = tests_received_list[0]
                tests_received = tests_received.replace(",", "")
                # check valid result
                if tests_received is '':
                    tests_received = "Error"


            else:
                tests_received = "Error"


            # has data been found?
            tests_positive = re.findall('(?:.* )(\\d*).?(\\d*)?%|(?:.* )', tests_received_line)
            if len(tests_positive) > 0:
                percent_digit, percent_decimal = tests_positive[0]
                if percent_digit == '' and not percent_decimal == '':
                    percent_digit = '0'

                percent_tests = percent_digit + "." + percent_decimal

                if percent_tests.endswith("."):
                    # trailing decimal place, remove the last character
                    percent_tests = percent_tests[:-1]

                # check valid result
                if percent_digit is '' and percent_decimal is '':
                    percent_tests = "Error"

            else:
                percent_tests = "Error"


            # find number of deaths
            deaths_list = re.findall('^([\\d,]+)(?:.)+', death_line)
            # has data been found
            if len(deaths_list) > 0:
                number_of_deaths = deaths_list[0].replace(",", "")
            else:
                number_of_deaths = "Error"

            # find number of people in intensive care
            intensive_care_list = re.findall('^([\\d,]+)(?:.)+', intensive_care_line)
            # has data been found
            if len(intensive_care_list) > 0:
                intensive_care_num = intensive_care_list[0].replace(",", "")
            else:
                intensive_care_num = "Error"

            # find number of people in hospital
            hospital_list = re.findall('^([\\d,]+)(?:.)+', hospital_line)
            # has data been found?
            if len(hospital_list) > 0:
                number_in_hospital = hospital_list[0].replace(",", "")
            else:
                number_in_hospital = "Error"

            # find number of vaccinations
            vacc_list = re.findall('([\\d,]*)(?:[^\\d]*)covid(?:-19)?(?:[^\\d]*)([\\d,]*)(?:[^\\d]*)second(?:[^\\d]*)([\\d,]*)(?:[^\\d]*)(?:.*)', vacc_line.lower())
            # has data been found?
            if len(vacc_list) > 0:
                first_dose, second_dose, third_dose = vacc_list[0]

                # remove commas
                first_dose = first_dose.replace(",", "")
                second_dose = second_dose.replace(",", "")
                third_dose = third_dose.replace(",", "")

                # check valid result
                if first_dose is '':
                    first_dose = "Error"
                if second_dose is '':
                    second_dose = "Error"
                if third_dose is '':
                    third_dose = "Error"

            else:
                first_dose = "Error"
                second_dose = "Error"
                third_dose = "Error"

            return date, number_of_cases, percent_cases, number_of_deaths, intensive_care_num, number_in_hospital, \
                   tests_received, percent_tests, first_dose, second_dose, third_dose
        except Exception as e:
            self.logger.error("Error in csv parsing. Error is: " + str(e))
            for id in self.owners:
                self.send_text(
                    user_id=id,
                    text="CSV error. Program *should* still be running though"
                )
            return None

        return None



    def log_gov_data(self, data, log=True, csv=True, data_path=None):
        if data_path is None:
            data_path = self.bot_files_dir
        # write to txt file for checking updated info
        if log:
            with open(os.path.join(data_path, "olddata.txt"), 'w') as data_file:
                data_file.write(data)

        # create csv, added try catch so even if it fails the program carries on as it isnt critical
        csv_data = self.extract_data(data)
        if isinstance(csv_data, tuple):
            date, number_of_cases, percent_cases, number_of_deaths, intensive_care_num, number_in_hospital, \
            tests_received, percent_tests, first_dose, second_dose, third_dose = csv_data
        else:
            if csv_data == -1:
                return None
            self.logger.error(f"Error in extracting CSV data, returned value is {csv_data}")
            return None
        data={'Date': date, 'Number of cases': number_of_cases, 'Percent Newly Positive': percent_cases,
                  'Deaths': number_of_deaths, 'People in Intensive Care': intensive_care_num,
                  'People in Hospital': number_in_hospital, 'Returned Tests': tests_received,
                  'Percent Positive Tests': percent_tests,
                  'First Vaccination Dose': first_dose, 'Second Vaccination Dose': second_dose,
                  'Third Vaccination Dose': third_dose}
        # log to csv file
        df = pd.DataFrame(data=data,
            index=[0])
        df.to_csv(path_or_buf=os.path.join(data_path, 'data.csv'), mode='a+', header=False, index=False)
        return data


    def daily_update(self, text, comparison = None):
        """ Sends updated information to all subscribers. """
        if text is None or text == "":
            return

        ids = []
        try:
            with open(os.path.join(self.bot_files_dir, 'subscribers.txt'), 'r') as subscriber_file:
                lines = subscriber_file.readlines()
                for line in lines:
                    line = line.strip().replace("\n", "")
                    if line == "":
                        continue
                    ids.append(int(line))
        except FileNotFoundError as e:
            self.logger.warning('No subscribers file found, creating blank file')
            Path(os.path.join(self.bot_files_dir, 'subscribers.txt'), mode="700").touch()

        formatted_text = ""
        if len(ids) > 0:
            for t in text.split("\n"):
                formatted_text += "{}\n".format(t.strip())

            formatted_text = formatted_text.strip()


        def replace_text(formatted_text,text_to_find, dictionary_key, is_before = True):
            if comparison and comparison[dictionary_key] is not None:
                if comparison[dictionary_key] > 0:
                    insert_text = "(+" + "{:,}".format(comparison[dictionary_key]) + ") " + text_to_find
                elif comparison[dictionary_key] < 0:
                    insert_text = "(" + "{:,}".format(comparison[dictionary_key]) + ") " + text_to_find
                else:
                    insert_text = "(NC) " + text_to_find
                formatted_text = formatted_text.replace(text_to_find,insert_text)
            return formatted_text


        formatted_text = replace_text(formatted_text,"new cases of COVID-19 reported","Number of cases")
        formatted_text = replace_text(formatted_text,"new tests for COVID-19 that reported results","Returned Tests")
        formatted_text = replace_text(formatted_text,"of these were positive","Percent Positive Tests")
        formatted_text = replace_text(formatted_text,"new reported death","Deaths")
        formatted_text = replace_text(formatted_text,"people were in intensive care yesterday with recently confirmed COVID-19","People in Intensive Care")
        formatted_text = replace_text(formatted_text,"people were in hospital yesterday with recently confirmed COVID-19","People in Hospital")
        formatted_text = replace_text(formatted_text,"people have received their first dose","First Vaccination Dose")
        formatted_text = replace_text(formatted_text,"have received their second dose","Second Vaccination Dose")
        formatted_text = replace_text(formatted_text,"have received a third","Third Vaccination Dose")


        for user_id in ids:
            self.bot.send_sticker(
                chat_id=user_id,
                sticker="CAACAgIAAxkBAAN4X2yBIhO2dsJ-Ul1hqWyvipNUpC0AAucBAAJWnb0KDXlZy_pIXAsbBA"
            )

            self.bot.send_text(
                user_id=user_id,
                text=formatted_text
            )
