
import os
import sys
import pathlib
import requests
import logging
import zipfile
import random
import traceback
import collections
import click
import re
import pandas as pd
from requests.exceptions import RequestException
from bs4 import BeautifulSoup
from threading import Thread
from time import sleep
from pathlib import Path
from datetime import datetime, timedelta

top_dir = pathlib.Path(__file__).absolute().parent.parent.as_posix()
sys.path.append(top_dir)

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

from src import config
from src.lib.graphing.graph import Graphing
from src.lib.exceptions.exceptions import GrapherNotFoundError
from src.lib.logging.telegramHandler import TelegramLogHandler
from src.scraper import Scraper

RESPONSES = ["ooo look at the big man talking to a bot that can't reply",
             "leave me alone",
             "blame the government"]

# Enable logging
LOG_DIR = os.path.join(os.getcwd(), 'logs')
LOG_FILENAME = os.path.join(LOG_DIR, 'rotating_log')
os.makedirs(LOG_DIR, exist_ok=True)
logging.addLevelName(31, 'MESG')
logging.addLevelName(35, 'RECMESG')
LOGGING_LEVELS = [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL, 31, 35]
LOG_FORMATTER = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')


def requires_owner(func):
    def handle(self, update, context):
        self.logger.info("Received privileged request from user {}: {}".format(update.effective_user.name,
                                                                               update.effective_message.text))
        if update.effective_user.id not in self.owners:
            self.logger.info("An invalid user attempted to use this service")
            update.effective_message.reply_text("You ain't an owner")
            return
        func(self, update, context)

    return handle


    return handle
def log_message(func):
    def handle(self, update, context):
        """ Log any message received """
        message = update.effective_message
        user = update.effective_user
        chat = update.effective_chat

        if chat.type == "private":
            chat_name = "Private Chat with " + user.name
        else:
            chat_name = chat.title

        self.logger.log(
            level=35,
            msg="User " + user.name + " (" + str(user.id) + ") has just sent a message in chat " + \
                chat_name + " (" + str(chat.id) + ")" + " saying \"" + \
                message.text + "\"."
        )

        func(self, update, context)
    return handle


class Bot:
    def __init__(self, should_log, bot_token, owners):
        # Setup telegram bot
        self.updater = Updater(bot_token, use_context=True)
        self.bot = self.updater.bot
        self.owners = owners

        if isinstance(owners, int):
            self.owners = (owners,)
        elif isinstance(owners, list):
            assert len(owners) > 0
            self.owners = tuple(owners)
        elif isinstance(owners, tuple):
            assert len(owners) > 0
            self.owners = owners
        else:
            raise TypeError("Owners should be of type: int, tuple, list")
        assert all(isinstance(x, int) for x in self.owners)

        # Setup logging
        self.logger = logging.getLogger(__name__)
        for logger_level in LOGGING_LEVELS:
            name = logging.getLevelName(logger_level)
            handler = logging.handlers.RotatingFileHandler(
                LOG_FILENAME + "." + name + ".LOG",
                maxBytes=config.max_log_bytes,
                backupCount=config.log_backup_count
            )
            handler.setFormatter(LOG_FORMATTER)
            handler.setLevel(logger_level)
            self.logger.addHandler(handler)
        # Add stdout (print) logger
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setFormatter(LOG_FORMATTER)
        stdout_handler.setLevel(logging.DEBUG)
        self.logger.addHandler(stdout_handler)

        self.bot_files_dir = os.path.join(os.getcwd(), 'bot_files')
        if not os.path.exists(self.bot_files_dir):
            os.mkdir(self.bot_files_dir)

        self.logger.setLevel(logging.DEBUG)

        # Add telegram logger
        for id in self.owners:
            telegram_logger = TelegramLogHandler(id, self.bot)
            telegram_logger.setLevel(logging.ERROR)
            telegram_logger.setFormatter(LOG_FORMATTER)
            self.logger.addHandler(telegram_logger)

        self.grapher = Graphing()
        self.plot_options = [
            "deaths",
            "cases",
            "intensive_care",
            "hospitalisations",
            "tests",
            "percent_tests",
            "first_vaccine",
            "second_vaccine",
            "third_vaccine"
        ]
        self.plot_buttons = [
            [
                InlineKeyboardButton("Cases", callback_data="cases"),
                InlineKeyboardButton("Deaths", callback_data="deaths")
            ],
            [
                InlineKeyboardButton("Hospitalisations", callback_data="hospitalisations"),
                InlineKeyboardButton("Intensive Care", callback_data="intensive_care")
            ],
            [
                InlineKeyboardButton("Tests", callback_data="tests"),
                InlineKeyboardButton("Positive Tests (%)", callback_data="percent_tests"),
            ],
            [
                InlineKeyboardButton("First Vaccine", callback_data="first_vaccine"),
                InlineKeyboardButton("Second Vaccine", callback_data="second_vaccine")
            ],
            [
                InlineKeyboardButton("Third Vaccine", callback_data="third_vaccine"),
                InlineKeyboardButton("Cancel", callback_data="cancel")
            ],
        ]

        self.scraper = Scraper(should_log, bot_token, self.logger, self, self.bot_files_dir)

    def run(self):
        self.scraper.run()
        for id in self.owners:
            self.send_text(id, text="Starting")

        dp = self.updater.dispatcher

        dp.add_handler(CommandHandler("start", self.start))
        dp.add_handler(CommandHandler("data", self.send_data))
        dp.add_handler(CommandHandler("logs", self.get_logs))
        dp.add_handler(CommandHandler("subscribe", self.subscribe))
        dp.add_handler(CommandHandler("unsubscribe", self.unsubscribe))
        dp.add_handler(CommandHandler("thread", self.thread))
        dp.add_handler(CommandHandler("status", self.status))
        dp.add_handler(CommandHandler("csv", self.get_csv))
        dp.add_handler(CommandHandler("clearcsv", self.clear_csv))
        dp.add_handler(CommandHandler("plot", self.plot))
        dp.add_handler(CommandHandler("announce", self.announce))
        dp.add_handler(CommandHandler("kill", self.kill_program))
        dp.add_handler(CommandHandler("goodnight", self.goodnight))
        dp.add_handler(CallbackQueryHandler(self.handle_callback))

        dp.add_handler(MessageHandler(Filters.text, self.text_log))

        self.updater.start_polling()

        self.updater.idle()

    # helper functions to send data
    def send_private_document(self, user_id, file, caption, filename):
        """ Generic method for sending documents to a user. """
        self.logger.log(
            level=31,
            msg="Bot sending document \"" + str(filename) +
                " with caption " + caption + "."
        )

        self.bot.send_document(
            chat_id=user_id,
            document=file,
            caption=caption,
            filename=filename
        )

    def send_text(self, user_id, text):
        """ Generic method for sending text to a user. """
        self.logger.log(
            level=31,
            msg="Bot sending message to ID " + str(user_id) + " saying " + text.replace("\n\n", "\n\t\t") + "."
        )

        self.bot.send_message(user_id, text)

    def send_sticker(self, chat_id, sticker):
        self.bot.send_sticker(
            chat_id=chat_id,
            sticker=sticker
        )


    # commands users can request

    @log_message
    def start(self, update, context):
        message_to_send = f"Hello, welcome to COVID bot. This bot collects the daily Scottish Government COVID stats " \
                          f"from gov.scot/coronavirusdata.\n\nCommands:\n" \
                          f"-To receive the current stats send /data\n\n" \
                          f"-To subscribe to the bot and receive updates within a minute of them being updated" \
                          f", send /subscribe (/unsubscribe to stop the daily updates).\n\n" \
                          f"-To view a plot of how the different stats have changed since 02/03/2020, send /plot and " \
                          f"follow the onscreen help\n\n" \
                          f"-To get a csv of all data since 02/03/2020, send /csv.\n\n"

        self.send_text(update.effective_user.id, message_to_send)

    @log_message
    def send_data(self, update, context):
        """ Provides data to a user if requested. """
        raw_html, status_code = self.scraper.simple_get(config.scot_url)
        data = self.scraper.get_scotland_info(raw_html)
        if data is None:
            data = "No valid data found"

        data_parsed = data
        data_formatted = ""
        if "\n\n" in data_parsed:
            data_parsed = data.replace("\n\n", "\n")
        data_parsed = data_parsed.split("\n")
        data_parsed[0] = data_parsed[0] + "\n\n"
        data_nice = ""
        for d in data_parsed:
            if d.strip() == "":
                continue
            data_formatted += d + "\n\n"

        data_formatted = data_formatted.strip()
        self.logger.info("User " + update.effective_user.name + " requested data. Data sent was: \n\t" + data_formatted)

        self.send_text(
            update.effective_user.id,
            text=data_formatted
        )

    @log_message
    def get_csv(self, update, context):
        """ Sends the CSV file containing all data to whoever requests it"""
        # log this
        self.logger.info("csv file requested.")
        path = os.path.join(self.bot_files_dir, 'data.csv')
        if os.path.isfile(path):
            self.send_private_document(
                update.effective_user.id,
                file=open(path, 'rb'),
                caption="All Data", filename='data.csv'
            )
        else:
            self.send_text(
                update.effective_user.id,
                "No CSV file exists."
            )

    @log_message
    def subscribe(self, update, context):
        """ Allows user to subscribe to daily updates. """
        user_id = update.effective_user.id

        # have they subscribed?
        line = ""
        with open(os.path.join(self.bot_files_dir, 'subscribers.txt'), 'r') as subscriber_file:
            lines = subscriber_file.readlines()
            for line in lines:
                line = line.rstrip()
                if line == str(user_id):
                    update.effective_message.reply_text("You are already subscribed.")
                    return

        # Add them to the list
        with open(os.path.join(self.bot_files_dir, 'subscribers.txt'), 'a+') as subscriber_file:
            to_write = str(user_id) + "\n"
            if not line == "":
                # last line in subscriber file wasnt empty, has a user id. make sure to go onto a new line
                to_write = "\n" + to_write
            subscriber_file.write(to_write)

        # Let them know
        self.send_text(user_id=update.effective_user.id, text="You are now subscribed.")
        self.logger.info(update.effective_user.name + " has subscribed")

        # Tell me someone has subsribed
        for id in self.owners:
            self.send_text(user_id=id, text=update.effective_user.name + " has just subscribed")

    @log_message
    def unsubscribe(self, update, context):
        """ Allows user to unsubscribe from daily updates. """
        user_id = update.effective_user.id

        ids = []
        found = False
        # find them
        with open(os.path.join(self.bot_files_dir, 'subscribers.txt'), 'r') as subscriber_file:
            lines = subscriber_file.readlines()
            for line in lines:
                line = line.rstrip()
                if line == "":
                    # ignore empty lines
                    continue

                if line == str(user_id):
                    found = True
                else:
                    ids.append(line)

        if found:
            with open(os.path.join(self.bot_files_dir, 'subscribers.txt'), 'w+') as subscriber_file:
                for user_id in ids:
                    subscriber_file.write(user_id + "\n")

            self.send_text(user_id=update.effective_user.id, text="You are now unsubscribed.")
            self.logger.info(update.effective_user.name + " has unsubscribed")

            # Tell me someone has unsubscribed
            for id in self.owners:
                self.send_text(user_id=id, text=update.effective_user.name + " has just unsubscribed")
        else:
            update.effective_message.reply_text("You weren't subscribed.")


    @log_message
    def plot(self, update, context):
        """Generate plot options keyboard"""
        markup = InlineKeyboardMarkup(self.plot_buttons)
        self.plot_message_id = update.message.reply_text(
            text="Ok, what do you want to plot?",
            reply_markup=markup
        )

    @requires_owner
    @log_message
    def status(self, update, context):
        user = update.effective_user

        is_alive = lambda alive: "alive" if alive else "dead"

        self.send_text(
            user.id,
            "The bot is currently online. The checker thread is " + is_alive(self.scraper.checker_thread.is_alive())
        )

    @log_message
    def text_log(self, update, context):
        """ Logs any non-command sent to the bot """
        message = update.effective_message
        user = update.effective_user
        chat = update.effective_chat

        triggers = ["fs", "fuck", "already"]
        for trigger in triggers:
            if trigger in message.text.lower():
                self.send_text(user_id=user.id, text=RESPONSES[random.randint(0, len(RESPONSES) - 1)])
                break

    # owner commands

    @requires_owner
    @log_message
    def announce(self, update, context):
        """ Sends an announce message to everyone from the owner """
        ids = []
        regexpr = '(?:\/[^ ]*)([\s\S]*)'
        message = re.findall(regexpr, update.effective_message.text)[0]
        if message == "":
            return
        try:
            with open(os.path.join(self.bot_files_dir, 'subscribers.txt'), 'r') as subscriber_file:
                lines = subscriber_file.readlines()
                for line in lines:
                    line = line.strip().replace("\n", "")
                    if line == "":
                        continue
                    ids.append(int(line))
        except FileNotFoundError as e:
            self.logger.warning('No subscribers file found, creating blank file')
            Path(os.path.join(self.bot_files_dir, 'subscribers.txt'), mode="700").touch()

        for user_id in ids:
            self.send_text(
                user_id=user_id,
                text="Bot message:\n\n" + message.strip()
            )

    @requires_owner
    @log_message
    def thread(self, update, context):
        """ Allows for manual restart of the checker thread. """
        if self.scraper.checker_thread.is_alive():
            self.logger.warning(
                update.effective_user.name + " just attempted to revive the checker thread. It was still alive.")
            update.effective_message.reply_text("Thread is still alive, blame the government.")
        else:
            self.logger.critical("Thread died and wasn't revived.")
            update.effective_message.reply_text("Restarted thread")
            for id in self.owners:
                self.send_text(id, text="Checked thread has just had a manual restart.")
            self.scraper.checker_thread = Thread(target=self.scraper.update_checker, daemon=False, args =(lambda : self.scraper.check_for_data, ))
            self.scraper.checker_thread.name = "Checker thread"
            self.scraper.checker_thread.start()


    @log_message
    @requires_owner
    def kill_program(self, update, context):
        self.logger.critical(f"{update.effective_user.name} has killed the program. "
                             f"This will need to be manually restarted")
        self.scraper.check_for_data = False
        self.scraper.checker_thread.join()
        sys.exit(-1)

    @log_message
    @requires_owner
    def goodnight(self, update, context):
        """ Sends updated information to all subscribers. """
        text = " Hello, the Scottish Government will no longer update the webpage this bot uses from Monday 11th April. Due to the large amount of changes required to fix this, and the general lifting of restrictions, the bot will now no longer send daily updates and will be offline. \n\nIf you want to make the changes yourself and run your own version of the bot, the source code can be found here - https://gitlab.com/markfalconer44/covid_bot.\n\nCheers for using the bot"

        ids = []
        try:
            with open(os.path.join(self.bot_files_dir, 'subscribers.txt'), 'r') as subscriber_file:
                lines = subscriber_file.readlines()
                for line in lines:
                    line = line.strip().replace("\n", "")
                    if line == "":
                        continue
                    ids.append(int(line))
        except FileNotFoundError as e:
            self.logger.warning('No subscribers file found, creating blank file')
            Path(os.path.join(self.bot_files_dir, 'subscribers.txt'), mode="700").touch()

        formatted_text = ""
        if len(ids) > 0:
            for t in text.split("\n"):
                formatted_text += "{}\n".format(t.strip())

            formatted_text = formatted_text.strip()

        for user_id in ids:
            self.send_text(
                user_id=user_id,
                text=text
            )

            self.send_sticker(
                chat_id=user_id,
                sticker="CAACAgIAAxkBAAM6YlA0EbastODfi_6QByapvm8TT7gAAs8BAAJWnb0K556oyRTYpoUjBA"
            )


    @requires_owner
    @log_message
    def clear_csv(self, update, context):
        """ Clears all data in CSV file, ensures only owner can do this """
        # Send zipped and most recent of each type of log file, to rank admin or above only

        # log this
        self.logger.info("Clearing CSV file.")

        # send current csv incase done by mistake
        self.send_text(update.effective_user.id, "Sending current CSV")
        self.get_csv(update, context)

        with open(os.path.join(self.bot_files_dir, 'data.csv'), 'w+') as csv_file:
            csv_file.write("Date,Number Of Cases,Percent Newly Positive,Deaths,People in Intensive Care,"
                           "People in Hospital,Returned Tests,Percent Positive Tests\n")

        raw_html, status_code = self.scraper.simple_get(config.scot_url)
        new_data = self.scraper.get_scotland_info(raw_html)
        self.scraper.log_gov_data(new_data)
        self.send_text(update.effective_user.id, "CSV data cleared. Todays info re-added")

    @requires_owner
    @log_message
    def get_logs(self, update, context):
        """ Allows logs to be requested. Ensures only owner can receive them """
        # Send zipped and most recent of each type of log file, to rank admin or above only

        # log this
        self.logger.info("logs requested.")

        # zip file
        self.zip_logs(update)

        # send zipped file
        try:
            log_zipped = open('logs.zip', 'rb')
        except Exception as e:
            self.logger.warning("An error occurred while trying to open logs.zip")
            self.logger.warning(str(e))
            self.logger.warning(traceback.format_exc())
            self.send_text(
                update.effective_user.id,
                "Sorry, something went wrong on our end!"
            )
            return
        self.send_private_document(
            update.effective_user.id,
            file=log_zipped,
            caption="All logs",
            filename='logs.zip'
        )

        # send logs
        logs = []
        for level in LOGGING_LEVELS:
            logs.append(logging.getLevelName(level))

        valid_log = []
        for log_type in logs:
            path = os.path.join(LOG_DIR, "rotating_log." + log_type + ".LOG")
            if os.path.getsize(path) > 0:
                valid_log.append(log_type)
            else:
                self.logger.info(log_type + " level log is empty")

        for log_type in valid_log:
            path = os.path.join(LOG_DIR, "rotating_log." + log_type + ".LOG")
            try:
                log_file = open(path, 'rb')
                self.send_private_document(
                    update.effective_user.id,
                    file=log_file,
                    caption="Most recent logs as " + log_type + " or higher",
                    filename=log_type + ".log"
                )
            except Exception as e:
                self.logger.warning("An error occurred while trying to open {}".format(path))
                self.logger.warning(str(e))
                self.logger.warning(traceback.format_exc())
                self.send_text(
                    update.effective_user.id,
                    "Sorry, something went wrong on our end!"
                )
                return

    # command helpers
    def handle_callback(self, update, context):
        """Handles callback request"""
        query = update.callback_query
        target = query.data
        if target in self.plot_options:
            self.logger.info("User " + update.effective_user.name + " has requested plot")
            self.do_plot(update, target)
            self.bot.deleteMessage(
                chat_id=query.message.chat.id,
                message_id=query.message.message_id
            )
        elif target == "cancel":
            self.bot.deleteMessage(
                chat_id=query.message.chat.id,
                message_id=query.message.message_id
            )
        else:
            self.logger.info("Unexpected callback query: {}".format(target))

    def do_plot(self, update, target):
        """Provides the requested plot to a user"""
        try:
            self.logger.info(f"User {update.effective_user.name} requested a graph of {target}.")
            filepath = self.grapher.graph(target)
        except GrapherNotFoundError as e:
            # Requested target is invalid
            self.logger.info("User requested an invalid plot type: {}".format(target))
            self.send_text(
                update.effective_user.id,
                "Invalid request, {} is not a valid target".format(target)
            )
            return
        except Exception as e:
            self.logger.warning("An unexpected error occurred while plotting")
            self.logger.warning(str(e))
            self.logger.warning(traceback.format_exc())
            self.send_text(
                update.effective_user.id,
                "Sorry, something went wrong on our end!"
            )
            return
        if not filepath:
            self.logger.warning("Grapher did not return a filepath")
            self.send_text(
                update.effective_user.id,
                "Sorry, something went wrong on our end!"
            )
            return
        try:
            self.bot.send_photo(
                update.effective_user.id,
                photo=open(filepath, 'rb')
            )
        except FileNotFoundError as e:
            self.logger.warning("Grapher returned a filepath that does not exist: {}".format(filepath))
            self.send_text(
                update.effective_user.id,
                "Sorry, something went wrong on our end!"
            )
        except Exception as e:
            self.logger.warning("An exception occured whilst trying to send an image")
            self.logger.warning(str(e))
            self.logger.warning(traceback.format_exc())
        try:
            os.remove(filepath)
        except Exception as e:
            # Don't care, just trying to reduce memory usage anyway
            return
        return

    def zip_logs(self, update):
        """ Send all log files to owner if requested."""
        # Get all logs
        file_paths = []
        for root, directories, files in os.walk('logs'):
            for filename in files:
                # Create the full filepath by using os module.
                file_path = os.path.join(root, filename)
                file_paths.append(file_path)

        self.logger.info(str(len(file_paths)) + " logs found (" + str(file_paths) + ").")

        self.send_text(update.effective_user.id, text=str(len(file_paths)) + " logs found.")
        # Zip all files
        zip_file = zipfile.ZipFile('logs.zip', 'w')
        with zip_file:
            # writing each file one by one
            for file in file_paths:
                zip_file.write(file)

        # Close file
        zip_file.close()



@click.command()
@click.argument('bot_token', type=click.STRING)
@click.argument('owners', type=click.INT, nargs=-1)
def run(bot_token, owners):
    """Run covid bot"""
    bot = Bot(True, bot_token, owners)
    bot.run()


if __name__ == '__main__':
    run()
