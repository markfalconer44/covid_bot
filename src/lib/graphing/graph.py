from inspect import isclass
import os
import pandas as pd
import numpy as np
from datetime import datetime as dt
from src.lib.exceptions.exceptions import GrapherNotFoundError


class Graphing:
    """
    A class to graph covid stats
    """

    def __init__(self):
        # All classes that can produce graphs (graph factory)
        self.graphers = {
            getattr(Graphing, x).name: getattr(Graphing, x)
            for x in Graphing.__dict__
            if isclass(getattr(Graphing, x))
               and issubclass(getattr(Graphing, x), self.Grapher)
               and hasattr(getattr(Graphing, x), 'name')
               and getattr(getattr(Graphing, x), 'name')
        }

    def graph(self, target, filepath="bot_files/data.csv"):
        """
        Using the target specified, returns a filepath to a temporary file with that graph

        Parameters
        ----------
        target : str
            type of graph to plot e.g. vaccinations
        filepath : str
            filepath from which to obtain the data file

        Raises
        ______
        FileNotFoundError
            data file doesnt not exist
        GrapherNotFoundError
            specified target does not exist

        Returns
        _______
        file
            filepath at which to find temporary PNG of produced graph
        """
        grapher_factory = self.graphers.get(target, None)

        if not grapher_factory:
            raise GrapherNotFoundError("No such target")

        grapher = grapher_factory()
        dateparse = lambda x: dt.strptime(x, '%d %B %Y')
        data = pd.read_csv(
            filepath,
            parse_dates=['Date'],
            date_parser=dateparse
        )
        plot = grapher.plot(data)
        plot.grid(axis='y', color="#dbdbdb")
        fig = plot.get_figure()
        outpath = os.path.join("/tmp", target + dt.now().strftime("%Y-%m-%d_%H:%M:%S") + ".png")
        fig.savefig(outpath)

        return outpath

    class Grapher:
        """
        Default Class for graphing classes
        """

    class _DeathsGrapher(Grapher):
        """
        Graphs the deaths from covid statistics

        """
        name = "deaths"

        def plot(self, data):
            """
            Plot the deaths from the given data

            Parameters
            ----------
            data : dataframe
                pandas dataframes with required stats

            Returns
            _______
            file
                filepath at which to find temporary PNG of produced graph
            """
            details = data[["Date", "Deaths"]]
            details["Deaths"] = details["Deaths"].replace("Error", np.nan)
            details.Deaths = details.Deaths.astype(float)
            details["Deaths (7 Day Rolling Average)"] = details.rolling(window=7).mean()
            plot = details.plot(
                x="Date",
                y=["Deaths", "Deaths (7 Day Rolling Average)"],
                kind="line",
                title="Daily COVID-19 Deaths",
                figsize=(12, 6)
            )
            return plot

    class _CasesGrapher(Grapher):
        """
        Graphs the number of cases from covid statistics

        """
        name = "cases"

        def plot(self, data):
            """
            Plot the number of cases from the given data

            Parameters
            ----------
            data : dataframe
                pandas dataframes with required stats

            Returns
            _______
            file
                filepath at which to find temporary PNG of produced graph
            """
            details = data[["Date", "Number Of Cases"]]
            details["Number Of Cases"] = details["Number Of Cases"].astype(float)
            details["Number Of Cases (7 Day Rolling Average)"] = details.rolling(window=7).mean()
            plot = details.plot(
                x="Date",
                y=["Number Of Cases", "Number Of Cases (7 Day Rolling Average)"],
                kind="line",
                title="Daily COVID-19 Cases",
                figsize=(12, 6)
            )
            return plot

    class _HospitalisationsGrapher(Grapher):
        """
        Graphs the number of hospitalisations from covid statistics

        """
        name = "hospitalisations"

        def plot(self, data):
            """
            Plot the number of hospitalisations from the given data

            Parameters
            ----------
            data : dataframe
                pandas dataframes with required stats

            Returns
            _______
            file
                filepath at which to find temporary PNG of produced graph
            """
            details = data[["Date", "People in Hospital"]]
            details["People in Hospital"] = details["People in Hospital"].replace("Error", np.nan)
            details["People in Hospital"] = details["People in Hospital"].astype(float)
            details["People in Hospital (7 Day Rolling Average)"] = details.rolling(window=7).mean()
            plot = details.plot(
                x="Date",
                y=[
                    "People in Hospital",
                    "People in Hospital (7 Day Rolling Average)"
                ],
                kind="line",
                title="Daily COVID-19 Hospitalisations",
                figsize=(12, 6)
            )
            return plot

    class _IntensiveCareGrapher(Grapher):
        """
        Graphs the number of intensive care patients from covid statistics

        """
        name = "intensive_care"

        def plot(self, data):
            """
            Plot the number of intensive care patients from the given data

            Parameters
            ----------
            data : dataframe
                pandas dataframes with required stats

            Returns
            _______
            file
                filepath at which to find temporary PNG of produced graph
            """
            details = data[["Date", "People in Intensive Care"]]
            details["People in Intensive Care"] = details["People in Intensive Care"].replace("Error", np.nan)
            details["People in Intensive Care"] = details["People in Intensive Care"].astype(float)
            details["People in Intensive Care (7 Day Rolling Average)"] = details.rolling(window=7).mean()
            plot = details.plot(
                x="Date",
                y=[
                    "People in Intensive Care",
                    "People in Intensive Care (7 Day Rolling Average)"
                ],
                kind="line",
                title="Daily COVID-19 Intensive Care Patients",
                figsize=(12, 6)
            )
            return plot

    class _TestsGrapher(Grapher):
        """
        Graphs the number of returned tests from covid statistics

        """
        name = "tests"

        def plot(self, data):
            """
            Plot the number of returned tests from the given data

            Parameters
            ----------
            data : dataframe
                pandas dataframes with required stats

            Returns
            _______
            file
                filepath at which to find temporary PNG of produced graph
            """
            details = data[["Date", "Returned Tests"]]
            details["Returned Tests"] = details["Returned Tests"].replace("Error", np.nan)
            details["Returned Tests"] = details["Returned Tests"].astype(float)
            details["Returned Tests (7 Day Rolling Average)"] = details.rolling(window=7).mean()
            plot = details.plot(
                x="Date",
                y=[
                    "Returned Tests",
                    "Returned Tests (7 Day Rolling Average)"
                ],
                title="Daily COVID-19 Returned Tests",
                kind="line",
                figsize=(12, 6)
            )
            return plot

    class _PercentTestsGrapher(Grapher):
        """
        Graphs the percentage of returned tests that are positive from covid statistics

        """
        name = "percent_tests"

        def plot(self, data):
            """
            Plots the percentage of returned tests that are positive from covid statistics

            Parameters
            ----------
            data : dataframe
                pandas dataframes with required stats

            Returns
            _______
            file
                filepath at which to find temporary PNG of produced graph
            """
            details = data[["Date", "Percent Positive Tests"]]
            details["Percent Positive Tests"] = details["Percent Positive Tests"].replace("Error", np.nan)
            details["Percent Positive Tests"] = details["Percent Positive Tests"].astype(float)
            details["Percent Positive Tests (7 Day Rolling Average)"] = details.rolling(window=7).mean()
            plot = details.plot(
                x="Date",
                y=[
                    "Percent Positive Tests",
                    "Percent Positive Tests (7 Day Rolling Average)"
                ],
                title="Daily Percentage of Positive Returned COVID-19 Tests",
                kind="line",
                figsize=(12, 6)
            )
            return plot

    class _FirstVaccineGrapher(Grapher):
        """
        Graphs the number of first vaccines from covid statistics

        """
        name = "first_vaccine"

        def plot(self, data):
            """
            Plots the number of first vaccines from covid statistics

            Parameters
            ----------
            data : dataframe
                pandas dataframes with required stats

            Returns
            _______
            file
                filepath at which to find temporary PNG of produced graph
            """
            details = data[["Date", "First Vaccination Dose"]]
            details["First Vaccination Dose"] = details["First Vaccination Dose"].replace("Error", np.nan)
            details["First Vaccination Dose"] = details["First Vaccination Dose"].astype(float)
            details["First Vaccination Dose (7 Day Rolling Average)"] = details.rolling(window=7).mean()
            plot = details.plot(
                x="Date",
                y=[
                    "First Vaccination Dose",
                    "First Vaccination Dose (7 Day Rolling Average)"
                ],
                title="Daily COVID-19 Vaccinations (First-Dose)",
                kind="line",
                figsize=(12, 6)
            )
            return plot

    class _SecondVaccineGrapher(Grapher):
        """
        Graphs the number of second vaccines from covid statistics

        """
        name = "second_vaccine"

        def plot(self, data):
            """
            Plots the number of second vaccines from covid statistics

            Parameters
            ----------
            data : dataframe
                pandas dataframes with required stats

            Returns
            _______
            file
                filepath at which to find temporary PNG of produced graph
            """
            details = data[["Date", "Second Vaccination Dose"]]
            details["Second Vaccination Dose"] = details["Second Vaccination Dose"].replace("Error", np.nan)
            details["Second Vaccination Dose"] = details["Second Vaccination Dose"].astype(float)
            details["Second Vaccination Dose (7 Day Rolling Average)"] = details.rolling(window=7).mean()
            plot = details.plot(
                x="Date",
                y=[
                    "Second Vaccination Dose",
                    "Second Vaccination Dose (7 Day Rolling Average)"
                ],
                title="Daily COVID-19 Vaccinations (Second-Dose)",
                kind="line",
                figsize=(12, 6)
            )
            return plot

    class _ThirdVaccineGrapher(Grapher):
        """
        Graphs the number of third vaccines from covid statistics

        """
        name = "third_vaccine"

        def plot(self, data):
            """
            Plots the number of second vaccines from covid statistics

            Parameters
            ----------
            data : dataframe
                pandas dataframes with required stats

            Returns
            _______
            file
                filepath at which to find temporary PNG of produced graph
            """
            details = data[["Date", "Third Vaccination Dose"]]
            details["Third Vaccination Dose"] = details["Third Vaccination Dose"].replace("Error", np.nan)
            details["Third Vaccination Dose"] = details["Third Vaccination Dose"].astype(float)
            details["Third Vaccination Dose (7 Day Rolling Average)"] = details.rolling(window=7).mean()
            plot = details.plot(
                x="Date",
                y=[
                    "Third Vaccination Dose",
                    "Third Vaccination Dose (7 Day Rolling Average)"
                ],
                title="Daily COVID-19 Vaccinations (Third-Dose)",
                kind="line",
                figsize=(12, 6)
            )
            return plot