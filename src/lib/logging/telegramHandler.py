import logging


class TelegramLogHandler(logging.StreamHandler):
    """
    Handler to send messages to telegram
    """
    def __init__(self, owner_id, bot):
        super().__init__()
        self.owner_id = owner_id
        self.bot = bot

    def emit(self, record):
        """Actually emit the record"""
        msg = self.format(record)
        self.bot.send_message(
            self.owner_id,
            msg
        )
