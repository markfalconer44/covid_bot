FROM python:3.7

COPY . /app

WORKDIR /app/

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

# RUN pip3 install /app/requirements.txt
CMD python -u /app/src/telegram_bot.py ${BOT_TOKEN} ${OWNER_ID}