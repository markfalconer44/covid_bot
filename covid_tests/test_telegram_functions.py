from unittest import TestCase
from unittest.mock import patch, mock_open, Mock, call
import sys
import pathlib
import os



from requests.exceptions import RequestException

from src.scraper import Scraper
from covid_tests.DummyClasses import *
from src.telegram_bot import Bot

topdir = pathlib.Path(__file__).absolute().parent.parent.as_posix()
sys.path.append(topdir)


class TestSendData(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", 0)
        bot.logger = Mock()
        scraper = bot.scraper
        scraper.simple_get = Mock()
        scraper.simple_get.return_value = "", ""
        scraper.get_scotland_info = Mock()

        self.bot = bot
        self.scraper = scraper

    @patch('telegram.bot.Bot.send_message')
    def test_sends_message(self, mock_send_message):
        self.bot.send_text(0, "hi")
        mock_send_message.assert_called_once()

    # Telegram covid_tests
    @patch('telegram.bot.Bot.send_message')
    def test_send_data(self, mock_send_message):
        self.scraper.get_scotland_info.return_value = "New data\nIs this"

        with patch('telegram.update.Update') as mock_update:
            mock_update.effective_user.id = -1
            self.bot.send_data(mock_update, None)
            mock_send_message.assert_called_with(-1, "New data\n\n\n\nIs this")

    # Telegram covid_tests
    @patch('telegram.bot.Bot.send_message')
    def test_send_data_double_nl(self, mock_send_message):
        self.scraper.get_scotland_info.return_value = "New data\n\nIs this"

        with patch('telegram.update.Update') as mock_update:
            mock_update.effective_user.id = -1
            self.bot.send_data(mock_update, None)
            mock_send_message.assert_called_with(-1, "New data\n\n\n\nIs this")

    @patch('telegram.bot.Bot.send_message')
    def test_send_data_none(self, mock_send_message):
        self.scraper.get_scotland_info.return_value = None

        with patch('telegram.update.Update') as mock_update:
            mock_update.effective_user.id = -1
            self.bot.send_data(mock_update, None)

            mock_send_message.assert_called_with(-1, "No valid data found")

    @patch('telegram.bot.Bot.send_message')
    def test_send_data_empty_line(self, mock_send_message):
        self.scraper.get_scotland_info.return_value = "New data\n \nIs this"

        with patch('telegram.update.Update') as mock_update:
            mock_update.effective_user.id = -1
            self.bot.send_data(mock_update, None)
            mock_send_message.assert_called_with(-1, "New data\n\n\n\nIs this")


    @patch('telegram.bot.Bot.send_message')
    def test_send_data_tab(self, mock_send_message):
        self.scraper.get_scotland_info.return_value = "New data\n \t \nIs this"

        with patch('telegram.update.Update') as mock_update:
            mock_update.effective_user.id = -1
            self.bot.send_data(mock_update, None)
            mock_send_message.assert_called_with(-1, "New data\n\n\n\nIs this")


class TestAnnounce(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", -1)

        bot.logger = Mock()
        bot.log_info = Mock()

        self.bot = bot

    @patch('src.telegram_bot.Bot.send_text')
    def test_standard_announce(self, mock_send_text):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/announce text", chat_type="chat_type")
        self.bot.announce(dummy_update, None)

        calls = [call(text="Bot message:\n\ntext", user_id=-1), call(text="Bot message:\n\ntext", user_id=-2)]
        mock_send_text.assert_has_calls(calls, any_order=False)

    @patch('src.telegram_bot.Bot.send_text')
    def test_no_message(self, mock_send_text):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/announce", chat_type="chat_type")
        self.bot.announce(dummy_update, None)

        mock_send_text.assert_not_called()


    @patch('src.telegram_bot.Bot.send_text')
    def test_new_line_message(self, mock_send_text):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/announce hi\ntest", chat_type="chat_type")
        self.bot.announce(dummy_update, None)

        calls = [call(text="Bot message:\n\nhi\ntest", user_id=-1), call(text="Bot message:\n\nhi\ntest", user_id=-2)]
        mock_send_text.assert_has_calls(calls, any_order=False)


    @patch('src.telegram_bot.Bot.send_text')
    def test_not_owner(self, mock_send_text):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/announce hi", chat_type="chat_type", user_id=-10)
        dummy_update.effective_message.reply_text = Mock()
        self.bot.announce(dummy_update, None)

        mock_send_text.assert_not_called()
        dummy_update.effective_message.reply_text.assert_called_with("You ain't an owner")


    @patch('builtins.open')
    @patch('pathlib.Path.touch')
    def test_update_subscriber_file_missing(self, mock_touch, mock_open):
        """ No subscriber file. Shouldn't send any messages as a new file has been made"""
        dummy_update = DummyUpdater(message_text="/announce hi", chat_type="chat_type")
        dummy_update.effective_message.reply_text = Mock()

        mock_open.side_effect = FileNotFoundError
        self.bot.announce(dummy_update, None)
        mock_touch.assert_called_once()
        dummy_update.effective_message.reply_text.assert_not_called()
        self.assertRaises(FileNotFoundError)


class TestStatus(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", 0)

        bot.logger = Mock()

        bot.owner = Mock()

        bot.log_info = Mock()

        self.bot = bot

    @patch('src.telegram_bot.Bot.send_text')
    def test_status_command(self, mock_send_text):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="text", chat_type="chat_type")
        self.bot.owners = [-1]
        self.bot.status(dummy_update, None)

        mock_send_text.assert_called_with(-1, "The bot is currently online. The checker thread is dead")

    def test_status_not_owner(self):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="text", chat_type="chat_type")
        dummy_update.effective_message.reply_text = Mock()
        self.bot.owner = [0]
        self.bot.status(dummy_update, None)

        dummy_update.effective_message.reply_text.assert_called_with("You ain't an owner")


class TestDailyUpdate(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", 0)

        bot.logger = Mock()
        bot.send_text = Mock()
        bot.owner = Mock()
        bot.log_info = Mock()
        self.scraper = bot.scraper
        self.bot = bot

    @patch('telegram.bot.Bot.send_sticker')
    def test_update_normal(self, mock_send_sticker):
        text = "hi"
        self.scraper.daily_update(text)

        calls = [call(user_id=-1, text=text), call(user_id=-2, text=text)]
        self.bot.send_text.assert_has_calls(calls, any_order=True)


    @patch('telegram.bot.Bot.send_sticker')
    def test_update_newlines(self, mock_send_sticker):
        text = "Hi\n2"
        self.scraper.daily_update(text)

        calls = [call(user_id=-1, text=text), call(user_id=-2, text=text)]
        self.bot.send_text.assert_has_calls(calls, any_order=True)

    @patch('telegram.bot.Bot.send_sticker')
    def test_update_trailing_newline(self, mock_send_sticker):
        text = "Hi\n2\n"
        desired_text = "Hi\n2"
        self.scraper.daily_update(text)

        calls = [call(user_id=-1, text=desired_text), call(user_id=-2, text=desired_text)]
        self.bot.send_text.assert_has_calls(calls, any_order=True)

    @patch('telegram.bot.Bot.send_sticker')
    def test_update_trailing_spaces(self, mock_send_sticker):
        text = "Hi\n2   "
        desired_text = "Hi\n2"
        self.scraper.daily_update(text)

        calls = [call(user_id=-1, text=desired_text), call(user_id=-2, text=desired_text)]
        self.bot.send_text.assert_has_calls(calls, any_order=True)

    @patch('telegram.bot.Bot.send_sticker')
    def test_update_trailing_spaces_newline(self, mock_send_sticker):
        text = "Hi\n2   \n hi"
        desired_text = "Hi\n2\nhi"
        self.scraper.daily_update(text)

        calls = [call(user_id=-1, text=desired_text), call(user_id=-2, text=desired_text)]
        self.bot.send_text.assert_has_calls(calls, any_order=True)

    def test_update_empty_string(self):
        self.scraper.daily_update("")

        self.bot.send_text.assert_not_called()


    @patch('telegram.bot.Bot.send_sticker')
    def test_update_none_text(self, mock_send_sticker):
        text = None
        self.scraper.daily_update(text)

        self.bot.send_text.assert_not_called()

    @patch('builtins.open')
    @patch('pathlib.Path.touch')
    def test_update_subscriber_file_missing(self, mock_touch, mock_open):
        """ No subscriber file. Shouldn't send any messages as a new file has been made"""
        mock_open.side_effect = FileNotFoundError
        text = "Hi"
        self.scraper.daily_update(text)

        self.bot.send_text.assert_not_called()

        mock_touch.assert_called_once()
        self.assertRaises(FileNotFoundError)


class TestThread(TestCase):
    def setUp(self):

        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", -1)
        bot.send_text = Mock()
        bot.logger = Mock()
        bot.log_info = Mock()

        scraper = bot.scraper
        scraper.checker_thread = Mock()
        scraper.checker_thread.start = Mock()

        self.bot = bot
        self.scraper = scraper

    @patch('threading.Thread.start')
    def test_thread_restart(self, mock_start_thread):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/thread", chat_type="chat_type")
        dummy_update.effective_message.reply_text = Mock()

        self.scraper.checker_thread.is_alive.return_value = False

        self.bot.thread(dummy_update, None)

        mock_start_thread.assert_called_once()

    @patch('threading.Thread.start')
    def test_thread_restart_text(self, mock_start_thread):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/thread test", chat_type="chat_type")
        dummy_update.effective_message.reply_text = Mock()

        self.scraper.checker_thread.is_alive.return_value = False

        self.bot.thread(dummy_update, None)

        mock_start_thread.assert_called_once()

    @patch('threading.Thread.start')
    def test_thread_restart_text_newlines(self, mock_start_thread):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/thread test\nhi\n", chat_type="chat_type")
        dummy_update.effective_message.reply_text = Mock()

        self.scraper.checker_thread.is_alive.return_value = False

        self.bot.thread(dummy_update, None)

        mock_start_thread.assert_called_once()


    @patch('threading.Thread.start')
    def test_thread_running(self, mock_start_thread):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/thread", chat_type="chat_type")
        dummy_update.effective_message.reply_text = Mock()

        self.scraper.checker_thread.is_alive.return_value = True

        self.bot.thread(dummy_update, None)

        mock_start_thread.assert_not_called()
        dummy_update.effective_message.reply_text.assert_called_with("Thread is still alive, blame the government.")

    @patch('threading.Thread.start')
    def test_thread_not_owner(self, mock_start_thread):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/thread", chat_type="chat_type", user_id=-2)
        dummy_update.effective_message.reply_text = Mock()

        self.scraper.checker_thread.is_alive.return_value = True

        self.bot.thread(dummy_update, None)

        mock_start_thread.assert_not_called()
        dummy_update.effective_message.reply_text.assert_called_with("You ain't an owner")


class TestKill(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", -1)

        bot.logger = Mock()
        bot.log_info = Mock()

        scraper = bot.scraper
        scraper.checker_thread = Mock()

        self.bot = bot
        self.scraper = scraper


    @patch('sys.exit')
    def test_standard(self, mock_exit):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/kill", chat_type="chat_type")
        self.bot.kill_program(dummy_update, None)

        calls = [call("name has killed the program. This will need to be manually restarted")]
        self.bot.logger.critical.assert_has_calls(calls, any_order=False)
        self.scraper.checker_thread.join.assert_called_once()
        mock_exit.assert_called_once_with(-1)

    @patch('sys.exit')
    def test_standard_other_text(self, mock_exit):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/kill temp text", chat_type="chat_type")
        self.bot.kill_program(dummy_update, None)

        calls = [call("name has killed the program. This will need to be manually restarted")]
        self.bot.logger.critical.assert_has_calls(calls, any_order=False)
        self.scraper.checker_thread.join.assert_called_once()
        mock_exit.assert_called_once_with(-1)

    @patch('sys.exit')
    def test_standard_other_text_newline(self, mock_exit):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/kill temp \ntext", chat_type="chat_type")
        self.bot.kill_program(dummy_update, None)

        calls = [call("name has killed the program. This will need to be manually restarted")]
        self.bot.logger.critical.assert_has_calls(calls, any_order=False)
        self.scraper.checker_thread.join.assert_called_once()
        mock_exit.assert_called_once_with(-1)

    @patch('src.telegram_bot.Bot.send_text')
    def test_not_owner(self, mock_send_text):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="/kill", chat_type="chat_type", user_id=-10)
        dummy_update.effective_message.reply_text = Mock()
        self.bot.kill_program(dummy_update, None)

        mock_send_text.assert_not_called()
        dummy_update.effective_message.reply_text.assert_called_with("You ain't an owner")


class TestBaseTelegram(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", 0)
        bot.logger = Mock()
        scraper = bot.scraper
        scraper.simple_get = Mock()
        scraper.simple_get.return_value = "", ""
        scraper.get_scotland_info = Mock()

        self.bot = bot
        self.scraper = scraper

    # test document is actually sent
    @patch('telegram.bot.Bot.send_document')
    def test_send_document(self, mock_send_document):
        self.bot.send_private_document(0, None, "", "")
        mock_send_document.assert_called_once()


class TestGetLogs(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", (1,))
        bot.logger = Mock()
        bot.zip_logs = Mock()
        bot.send_private_document = Mock()
        bot.send_text = Mock()

        self.bot = bot
        self.scraper = bot.scraper


    @patch('builtins.open', mock_open())
    @patch('os.path.getsize')
    def test_get_logs_good_owner(self, mock_get_size):
        """Test get logs with good owner id"""
        dummy_update = DummyUpdater(message_text="Hello", chat_type="private", user_id=1)
        mock_get_size.return_value = 1
        self.bot.get_logs(dummy_update, None)
        self.bot.logger.warning.assert_not_called()
        call_count = self.bot.send_private_document.call_count
        self.assertEqual(8, call_count)

    @patch('builtins.open', mock_open())
    @patch('os.path.getsize')
    def test_get_logs_bad_owner(self, mock_get_size):
        """Test get logs with good owner id"""

        dummy_update = DummyUpdater(message_text="Hello", chat_type="private")
        dummy_update.effective_message.reply_text = Mock()
        mock_get_size.return_value = 1
        self.bot.get_logs(dummy_update, None)
        self.bot.logger.info.assert_called_with("An invalid user attempted to use this service")
        self.bot.send_private_document.assert_not_called()

    @patch('builtins.open')
    @patch('os.path.getsize')
    def test_get_logs_open_exception(self, mock_get_size, mock_open):
        """Test get logs with good owner id"""
        mock_open.side_effect = FileNotFoundError
        dummy_update = DummyUpdater(message_text="Hello", chat_type="private", user_id=1)
        mock_get_size.return_value = 1
        self.bot.get_logs(dummy_update, None)
        self.bot.logger.warning.assert_called()
        self.bot.send_text.assert_called_with(1, "Sorry, something went wrong on our end!")
        self.bot.send_private_document.assert_not_called()

    @patch('builtins.open', mock_open())
    @patch('os.path.getsize')
    def test_get_logs_zero_file_length(self, mock_get_size):
        """Test get logs with good owner id"""
        dummy_update = DummyUpdater(message_text="Hello", chat_type="private", user_id=1)
        mock_get_size.return_value = 0
        self.bot.get_logs(dummy_update, None)
        self.bot.logger.warning.assert_not_called()
        self.assertEqual(1, self.bot.send_private_document.call_count)

    @patch('builtins.open')
    @patch('os.path.getsize')
    def test_get_logs_open_second_exception(self, mock_get_size, mock_open):
        """Test get logs with good owner id"""
        mock_open.side_effect = [1, FileNotFoundError]
        dummy_update = DummyUpdater(message_text="Hello", chat_type="private", user_id=1)
        mock_get_size.return_value = 1
        self.bot.get_logs(dummy_update, None)
        self.assertEqual(3, self.bot.logger.warning.call_count)
        self.assertEqual(1, self.bot.send_private_document.call_count)


class TestSubscribe(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", 0)
        bot.logger = Mock()
        bot.send_text = Mock()

        self.bot = bot

    def test_normal(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            self.bot.subscribe(dummy_update, None)

            m_open().write.assert_called_with("-1\n")

    def test_already_subscribed(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["-1\n"]
            self.bot.subscribe(dummy_update, None)

            dummy_update.effective_message.reply_text.assert_called_with("You are already subscribed.")

    def test_already_subscribed_no_nl(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["-1"]
            self.bot.subscribe(dummy_update, None)

            dummy_update.effective_message.reply_text.assert_called_with("You are already subscribed.")

    def test_no_empty_last_line(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["-2"]
            self.bot.subscribe(dummy_update, None)

            m_open().write.assert_called_with("\n-1\n")


    def test_empty_sub_file(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = []
            self.bot.subscribe(dummy_update, None)

            m_open().write.assert_called_with("-1\n")

    def test_owners_told(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            self.bot.subscribe(dummy_update, None)

            self.bot.send_text.assert_called_with(user_id=0, text="name has just subscribed")

    def test_only_owners_told(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["-2"]
            self.bot.subscribe(dummy_update, None)

            calls = [call(user_id=-1, text="You are now subscribed."),
                     call(user_id=0, text="name has just subscribed")]

            self.bot.send_text.assert_has_calls(calls)

            self.assertEqual(2, self.bot.send_text.call_count)

    def test_logged(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            self.bot.subscribe(dummy_update, None)

            self.bot.logger.info.assert_called_with("name has subscribed")


class TestUnsubscribe(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", 0)
        bot.logger = Mock()
        bot.send_text = Mock()

        self.bot = bot

    def test_normal(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["-1\n", "-2\n"]
            self.bot.unsubscribe(dummy_update, None)

            m_open().write.assert_called_with("-2\n")

    def test_checks_exact_id(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["-10\n", "-2\n"]
            self.bot.unsubscribe(dummy_update, None)

            dummy_update.effective_message.reply_text.assert_called_with("You weren't subscribed.")

    def test_not_subscribed(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["-2\n"]
            self.bot.unsubscribe(dummy_update, None)

            dummy_update.effective_message.reply_text.assert_called_with("You weren't subscribed.")

    def test_handles_extra_newlines(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["\n", "\n", "-2\n", "\n", "-1\n"]
            self.bot.unsubscribe(dummy_update, None)

            m_open().write.assert_called_with("-2\n")

    def test_handles_id_twice(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["\n", "\n", "-1\n", "\n", "-1\n"]
            self.bot.unsubscribe(dummy_update, None)

            m_open().write.assert_not_called()


    def test_empty_sub_file(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = []
            self.bot.unsubscribe(dummy_update, None)

            dummy_update.effective_message.reply_text.assert_called_with("You weren't subscribed.")

    def test_owners_told(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["-1\n", "-2\n"]
            self.bot.unsubscribe(dummy_update, None)

            self.bot.send_text.assert_called_with(user_id=0, text="name has just unsubscribed")

    def test_only_owners_told(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["-1\n", "-2\n"]
            self.bot.unsubscribe(dummy_update, None)

            calls = [call(user_id=-1, text="You are now unsubscribed."),
                     call(user_id=0, text="name has just unsubscribed")]

            self.bot.send_text.assert_has_calls(calls)

            self.assertEqual(2, self.bot.send_text.call_count)

    def test_logged(self):
        dummy_update = DummyUpdater(message_text="/subscribe", chat_type="chat_type")
        with patch('builtins.open', mock_open()) as m_open:
            m_open().readlines.return_value = ["-1\n", "-2\n"]
            self.bot.unsubscribe(dummy_update, None)

            self.bot.logger.info.assert_called_with("name has unsubscribed")

