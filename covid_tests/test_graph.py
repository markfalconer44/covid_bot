from unittest import TestCase
import sys
import os
import pathlib
import re
import pandas as pd
from datetime import datetime as dt

from src.lib.graphing.graph import Graphing
from src.lib.exceptions.exceptions import GrapherNotFoundError

topdir = pathlib.Path(__file__).absolute().parent.parent.as_posix()
sys.path.append(topdir)


class TestGraphing(TestCase):
    """
    Unit Tests for src/lib/graph.py
    """
    def setUp(self):
        """Setup tests"""
        self.grapher = Graphing()
        dateparse = lambda x: dt.strptime(x, '%d %B %Y')
        self.data = pd.read_csv("data.csv", parse_dates=['Date'], date_parser=dateparse)

    def test_graph_all_good(self):
        """Test returns a valid graph when all is good"""
        graph_types = [
            "deaths",
            "cases",
            "hospitalisations",
            "tests",
            "percent_tests",
            "first_vaccine",
            "second_vaccine"
        ]
        for graph in graph_types:
            filepath = self.grapher.graph(graph, "data.csv")
            self.assertTrue(re.match('^/tmp/{}.*$'.format(graph), filepath))
            os.remove(filepath)

    def test_graph_bad_target(self):
        """Test grapher raises error when invalid target is passed"""
        self.assertRaises(GrapherNotFoundError, self.grapher.graph, "nonsense")

    def test_bad_filepath(self):
        """Test the correct error is raised when a bad filepath is supplied"""
        self.assertRaises(FileNotFoundError, self.grapher.graph, "deaths", "/thisfiledoesnotexist")




