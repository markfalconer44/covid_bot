import sys
import pathlib
topdir = pathlib.Path(__file__).absolute().parent.parent.as_posix()
sys.path.append(topdir)
import unittest
from covid_tests.test_scraper import *
from covid_tests.test_graph import TestGraphing
from covid_tests.test_telegram_functions import *
import os

# todo: test owner privileges

if __name__ == '__main__':
    # print(sys.path)
    os.chdir(topdir + "/covid_tests")
    os.makedirs(os.path.join(os.getcwd(), "logs"))
    # print(os.getcwd())
    unittest.main()
    os.rmdir(os.path.join(os.getcwd(), "logs"))
