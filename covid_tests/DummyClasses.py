from unittest.mock import Mock


class DummyUpdater:
    def __init__(self, message_text, chat_type, user_id=-1):
        self.effective_message = DummyMessage(message_text)
        self.effective_user = DummyUser("name", user_id)
        self.effective_chat = DummyChat(chat_type, "name", user_id)


class DummyMessage:
    def __init__(self, txt):
        self.text = txt
        self.reply_text = Mock()


class DummyUser:
    def __init__(self, name, id):
        self.name = name
        self.id = id


class DummyChat:
    def __init__(self, chat_type, title, chat_id):
        self.type = chat_type
        self.title = title
        self.id = chat_id

class DummyHTML:
    def __init__(self, date, data_list, div):
        self.content = "<div class=\"" + div + "\">"
        self.content = self.content + date + data_list + "</div>"

    def add_para(self, asteriks="*", content="random info"):
        self.content = self.content[:-6]
        self.content += f"<p><span>{asteriks}{content}</span></p>"
        self.content += "</div>"

    def as_string(self):
        return bytes(self.content, 'utf-8')
