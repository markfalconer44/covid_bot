from unittest import TestCase
from unittest.mock import patch, mock_open, Mock
import sys
import pathlib
import os

from requests.exceptions import RequestException

from src.scraper import Scraper
from covid_tests.DummyClasses import *

topdir = pathlib.Path(__file__).absolute().parent.parent.as_posix()
sys.path.append(topdir)

from src.telegram_bot import Bot

class TestOwnersInput(TestCase):
    def test_init_scraper_owner_is_int(self):
        """Test create scraper object with owner as int"""
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", 1)
        self.assertTrue(isinstance(bot, Bot))
        self.assertTrue(isinstance(bot.scraper, Scraper))

    def test_init_scraper_owner_is_list(self):
        """Test create scraper object with owners as list"""
        owners = [1, 2, 3, 4]
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", owners)
        self.assertTrue(isinstance(bot, Bot))
        self.assertTrue(isinstance(bot.scraper, Scraper))

    def test_init_scraper_owner_is_tuple(self):
        """Test create scraper object with owners as tuple"""
        owners = (1, 2, 3, 4)
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", owners)
        self.assertTrue(isinstance(bot, Bot))
        self.assertTrue(isinstance(bot.scraper, Scraper))

    def test_init_scraper_owner_is_bad(self):
        """Test create scraper object with owners as invalid type"""
        self.assertRaises(TypeError, Bot, False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", "bad")

    def test_init_scraper_owner_list_contains_bad_value(self):
        """Test create scraper object with owner list containing invalid type"""
        owners = [1, 2, 3, "bad"]
        self.assertRaises(AssertionError, Bot, False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", owners)

    def test_init_scraper_owner_tuple_contains_bad_value(self):
        """Test create scraper object with owner tuple containing invalid type"""
        owners = (1, 2, 3, "bad")
        self.assertRaises(AssertionError, Bot, False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", owners)

    def test_init_scraper_owner_tuple_is_empty(self):
        """Test create scraper object with empty owners tuple"""
        owners = ()
        self.assertRaises(AssertionError, Bot, False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", owners)

    def test_init_scraper_owner_list_is_empty(self):
        """Test create scraper object with empty owners list"""
        owners = []
        self.assertRaises(AssertionError, Bot, False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", owners)

class TestLogGovData(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", (0,))
        self.scraper = bot.scraper

    @patch('pandas.DataFrame')
    def test_csv_log_expected(self, mock_df):
        fake_gov_data = create_dummy_data()
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values()
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_missing_date_line(self, mock_df):
        fake_gov_data = create_dummy_data(date="Error", remove=0)
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(date="Error")
        mock_df.assert_called_with(data=values, index=[0])
        self.scraper.logger.warning.assert_called_once()

    @patch('pandas.DataFrame')
    def test_csv_log_missing_cases_line(self, mock_df):
        fake_gov_data = create_dummy_data(cases="Error", perc="Error", remove=1)
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(cases="Error", perc="Error")
        mock_df.assert_called_with(data=values, index=[0])
        self.scraper.logger.warning.assert_called_once()

    @patch('pandas.DataFrame')
    def test_csv_log_missing_death_line(self, mock_df):
        fake_gov_data = create_dummy_data(deaths="Error", remove=3)
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(deaths="Error")
        mock_df.assert_called_with(data=values, index=[0])
        self.scraper.logger.warning.assert_called_once()

    @patch('pandas.DataFrame')
    def test_csv_log_missing_intensive_line(self, mock_df):
        fake_gov_data = create_dummy_data(intens="Error", remove=4)
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(intens="Error")
        mock_df.assert_called_with(data=values, index=[0])
        self.scraper.logger.warning.assert_called_once()

    @patch('pandas.DataFrame')
    def test_csv_log_missing_hospital_line(self, mock_df):
        fake_gov_data = create_dummy_data(hospit="Error", remove=5)
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(hospit="Error")
        mock_df.assert_called_with(data=values, index=[0])
        self.scraper.logger.warning.assert_called_once()

    @patch('pandas.DataFrame')
    def test_csv_log_missing_tests_line(self, mock_df):
        fake_gov_data = create_dummy_data(tests="Error", remove=2)
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(tests="Error", test_perc="Error")
        mock_df.assert_called_with(data=values, index=[0])
        self.scraper.logger.warning.assert_called_once()

    @patch('pandas.DataFrame')
    def test_csv_log_cases_comma(self, mock_df):
        fake_gov_data = create_dummy_data(cases="12,34,567")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(cases="1234567")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_deaths_comma(self, mock_df):
        fake_gov_data = create_dummy_data(deaths="12,34,567")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(deaths="1234567")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_intensive_care_comma(self, mock_df):
        fake_gov_data = create_dummy_data(intens="12,34,567")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(intens="1234567")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_hospital_comma(self, mock_df):
        fake_gov_data = create_dummy_data(hospit="12,34,567")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(hospit="1234567")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_comma(self, mock_df):
        fake_gov_data = create_dummy_data(tests="12,34,567")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(tests="1234567")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_cases_asterisk(self, mock_df):
        fake_gov_data = create_dummy_data(cases="12,345*")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(cases="12345")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_perc_expected(self, mock_df, ):
        fake_gov_data = create_dummy_data(test_perc="5.7")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(test_perc="5.7")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_perc_expected_double_digits(self, mock_df, ):
        fake_gov_data = create_dummy_data(test_perc="15.7")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(test_perc="15.7")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_perc_missing(self, mock_df):
        fake_gov_data = create_dummy_data(test_perc="")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(test_perc="Error")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_perc_missing_digit(self, mock_df):
        fake_gov_data = create_dummy_data(test_perc=".7")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(test_perc="0.7")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_perc_missing_digit_double(self, mock_df):
        fake_gov_data = create_dummy_data(test_perc=".51")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(test_perc="0.51")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_perc_missing_decimal(self, mock_df):
        fake_gov_data = create_dummy_data(test_perc="5")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(test_perc="5")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_perc_missing_decimal_double_digit(self, mock_df):
        fake_gov_data = create_dummy_data(test_perc="12")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(test_perc="12")
        mock_df.assert_called_with(data=values, index=[0])


    @patch('pandas.DataFrame')
    def test_csv_log_missing_date_num(self, mock_df):
        fake_gov_data = create_dummy_data(date="")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(date="Error")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_missing_cases_rec_perc(self, mock_df):
        fake_gov_data = create_dummy_data(cases="Error")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(cases="Error")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_missing_perc_rec_cases(self, mock_df):
        fake_gov_data = create_dummy_data(perc="Error")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(perc="No longer provided")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_missing_deaths_num(self, mock_df):
        fake_gov_data = create_dummy_data(deaths="No date for 0")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(deaths="Error")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_missing_intensive_care_num(self, mock_df):
        fake_gov_data = create_dummy_data(intens="Error")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(intens="Error")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_missing_hospital_num(self, mock_df):
        fake_gov_data = create_dummy_data(hospit="Error")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(hospit="Error")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_missing_tests_num(self, mock_df):
        fake_gov_data = create_dummy_data(tests="Error")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(tests="Error")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_f_dose_asterisk(self, mock_df):
        fake_gov_data = create_dummy_data(dose_1="12,345*")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(dose_1="12345")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_f_dose_expected(self, mock_df):
        fake_gov_data = create_dummy_data(dose_1="12,345")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(dose_1="12345")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_f_dose_missing(self, mock_df):
        fake_gov_data = create_dummy_data(dose_1="")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(dose_1="Error")
        mock_df.assert_called_with(data=values, index=[0])


    @patch('pandas.DataFrame')
    def test_csv_log_tests_vacc_missing_dash_19(self, mock_df):
        # the gov like to sometimes omit the "-19" from covid-19 sometimes, make sure it still works
        fake_gov_data = create_dummy_data(dash_19=False)
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values()
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_s_dose_asterisk(self, mock_df):
        fake_gov_data = create_dummy_data(dose_2="12,345*")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(dose_2="12345")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_s_dose_expected(self, mock_df):
        fake_gov_data = create_dummy_data(dose_2="12,345")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(dose_2="12345")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_s_dose_missing(self, mock_df):
        fake_gov_data = create_dummy_data(dose_2="")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(dose_2="Error")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_t_dose_asterisk(self, mock_df):
        fake_gov_data = create_dummy_data(dose_3="12,345*")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(dose_3="12345")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_t_dose_expected(self, mock_df):
        fake_gov_data = create_dummy_data(dose_3="12,345")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(dose_3="12345")
        mock_df.assert_called_with(data=values, index=[0])

    @patch('pandas.DataFrame')
    def test_csv_log_tests_t_dose_missing(self, mock_df):
        fake_gov_data = create_dummy_data(dose_3="")
        self.scraper.logger = Mock()
        self.scraper.log_gov_data(fake_gov_data, log=False)
        values = create_dummy_expected_values(dose_3="Error")
        mock_df.assert_called_with(data=values, index=[0])


class TestGetScotlandInfo(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", (0,))
        self.scraper = bot.scraper


    @patch('bs4.BeautifulSoup')
    def test_get_scotland_info_none_html(self, mock_html):
        end_value = self.scraper.get_scotland_info(None)
        self.assertIsNone(end_value)
        mock_html.assert_not_called()

    @patch('bs4.BeautifulSoup.find_all')
    def test_get_scotland_info_empty_html(self, mock_html):
        # mock_html.return_value = "
        end_value = self.scraper.get_scotland_info(b"")
        self.assertIs(end_value, "")
        mock_html.assert_called_once()

    @patch('src.config.scot_div')
    def test_get_scotland_info_valid_html(self, mock_div):
        # define fake content
        div = "fake_div"
        date = "mocked date"
        data_list = "mocked data"

        # what is expected?
        expected_result = date + "\n\n\n\n" + data_list

        # mock div value and create fake html
        mock_div.return_value = div
        content = DummyHTML(date="<h2>" + date + "</h2>", data_list=f"<ul><li><span>{data_list}</span></li></ul>", div=div)
        html = content.as_string()

        # call functio
        end_value = self.scraper.get_scotland_info(html)
        self.assertEqual(end_value, expected_result)

    @patch('src.config.scot_div')
    def test_get_scotland_two_bullets(self, mock_div):
        # define fake content
        div = "fake_div"
        date = "mocked date"
        data_list = ["mocked data", "data2"]

        # what is expected?
        expected_result = date + "\n\n\n\n" + data_list[0] + " \n\n" + data_list[1]

        # mock div value and create fake html
        mock_div.return_value = div
        content = DummyHTML(date="<h2>" + date + "</h2>", data_list=f"<ul>"
                                                                    f"<li><span>{data_list[0]}</span></li>\n"
                                                                    f"<li><span>{data_list[1]}</span></li></ul>", div=div)
        html = content.as_string()

        # call functio
        end_value = self.scraper.get_scotland_info(html)
        self.assertEqual(end_value, expected_result)

    @patch('src.config.scot_div')
    def test_get_scotland_info_no_close_tag(self, mock_div):
        # define fake content
        div = "fake_div"
        date = "mocked date"
        data_list = "mocked data"

        # mock div value and create fake, incorrect,  html
        mock_div.return_value = div
        content = DummyHTML(date="<h2>" + date + "<h2>", data_list="<ul><li>" + data_list + "</li></ul>", div=div)
        html = content.as_string()

        # call functio
        end_value = self.scraper.get_scotland_info(html)
        self.assertIsNone(end_value)

    @patch('src.config.scot_div')
    def test_get_scotland_info_no_ul(self, mock_div):
        # define fake content
        div = "fake_div"
        date = "mocked date"

        # mock div value and create fake, incorrect,  html
        mock_div.return_value = div
        content = DummyHTML(date="<h2>" + date + "</h2>", data_list="", div=div)
        html = content.as_string()

        # call functio
        end_value = self.scraper.get_scotland_info(html)
        self.assertIsNone(end_value)

    @patch('src.config.scot_div')
    def test_get_scotland_info_no_h2(self, mock_div):
        # define fake content
        div = "fake_div"
        data_list = "mocked data"

        # mock div value and create fake, incorrect,  html
        mock_div.return_value = div
        content = DummyHTML(date="", data_list=f"<ul><li><span>{data_list}</span></li></ul>", div=div)
        html = content.as_string()

        # call function
        end_value = self.scraper.get_scotland_info(html)
        self.assertEqual(end_value, data_list)

    @patch('src.config.scot_div')
    def test_whitespace_span(self, mock_div):
        """ Error happens when there is whitespace inbetween two span elements"""
        # define fake content
        div = "fake_div"
        data_list = ["mocked", "data"]

        # mock div value and create fake, incorrect,  html
        mock_div.return_value = div
        content = DummyHTML(date="", data_list=f"<ul><li><span>{data_list[0]} </span> <span>{data_list[1]}</span></li></ul>", div=div)
        html = content.as_string()

        # call function
        end_value = self.scraper.get_scotland_info(html)
        ev = end_value.replace("\n", "\\n")
        print(f"Received: {ev}")
        self.assertEqual(f"{data_list[0]} {data_list[1]}", end_value)


    @patch('src.config.scot_div')
    def test_get_scotland_info_sub_bullet_point(self, mock_div):
        # define fake content
        div = "fake_div"
        date = "mocked date"
        data_list = ["mocked data", "data2", "data3"]

        # what is expected?
        expected_result = f"{date}\n\n\n\n{data_list[0]} {data_list[1]} \n\n{data_list[2]}"

        bullet_points = f"<ul><li><span>{data_list[0]}</span>\n" + \
                        f"<ul><li><span>{data_list[1]}</span></li></ul></li>\n" +\
                        f"<li><span>{data_list[2]}</span></li></ul>"


        # mock div value and create fake, incorrect,  html
        mock_div.return_value = div
        content = DummyHTML(date="<h2>" + date + "</h2>", data_list=bullet_points, div=div)
        html = content.as_string()

        # call function
        end_value = self.scraper.get_scotland_info(html)
        ev = end_value.replace("\n", "NL")
        print(f"Received: {ev}")
        self.assertEqual(expected_result, end_value)

    @patch('src.config.scot_div')
    def test_get_scotland_info_no_date(self, mock_div):
        # define fake content
        div = "fake_div"
        date = "mocked date"
        data_list = "mocked data"

        # what is expected?
        expected_result = data_list

        # mock div value and create fake, incorrect,  html
        mock_div.return_value = div
        content = DummyHTML(date="", data_list=f"<ul><li><span>{data_list}</span></li></ul>", div=div)
        html = content.as_string()

        # call functio
        end_value = self.scraper.get_scotland_info(html)
        self.assertIsNotNone(end_value)
        self.assertEqual(expected_result, end_value)

    @patch('src.config.scot_div')
    def test_get_asterik_para_normal(self, mock_div):
        # define fake content
        div = "fake_div"
        date = "mocked date"
        data = "mocked* data"
        other_para = "para info"

        # what is expected?
        expected_result = f"{date}\n\n\n\n{data} \n\n\n\n------\n\n\n\n* {other_para}"

        bullet_points = f"<ul><li><span>{data}</span></li></ul>"


        # mock div value and create fake, incorrect,  html
        mock_div.return_value = div
        content = DummyHTML(date="<h2>" + date + "</h2>", data_list=bullet_points, div=div)
        content.add_para(content=other_para)
        html = content.as_string()

        # call function
        end_value = self.scraper.get_scotland_info(html)
        print(f"Received {end_value}")
        self.assertEqual(end_value, expected_result)

    @patch('src.config.scot_div')
    def test_get_asterik_para_two(self, mock_div):
        # define fake content
        div = "fake_div"
        date = "mocked date"
        data = "mocked* data**"
        other_para = ["para info", "other_para"]

        # what is expected?
        expected_result = f"{date}\n\n\n\n{data} \n\n\n\n------\n\n\n\n* {other_para[0]}\n\n\n\n** {other_para[1]}"

        bullet_points = f"<ul><li><span>{data}</span></li></ul>"


        # mock div value and create fake, incorrect,  html
        mock_div.return_value = div
        content = DummyHTML(date="<h2>" + date + "</h2>", data_list=bullet_points, div=div)
        content.add_para("*", other_para[0])
        content.add_para("**", other_para[1])
        html = content.as_string()

        # call function
        end_value = self.scraper.get_scotland_info(html)
        print(f"Received {end_value}")
        self.assertEqual(end_value, expected_result)

    @patch('src.config.scot_div')
    def test_get_asterik_not_in_text(self, mock_div):
        """
        Used incase we find a paragraph refering to an asteriks thing that isnt referenced in the text
        we pull
        """
        # define fake content
        div = "fake_div"
        date = "mocked date"
        data = "mocked* data"
        other_para = ["para info", "other_para"]

        # what is expected?
        expected_result = f"{date}\n\n\n\n{data} \n\n\n\n------\n\n\n\n* {other_para[0]}"

        bullet_points = f"<ul><li><span>{data}</span></li></ul>"


        # mock div value and create fake, incorrect,  html
        mock_div.return_value = div
        content = DummyHTML(date="<h2>" + date + "</h2>", data_list=bullet_points, div=div)
        content.add_para("*", other_para[0])
        content.add_para("**", other_para[1])
        html = content.as_string()

        # call function
        end_value = self.scraper.get_scotland_info(html)
        print(f"Received {end_value}")
        self.assertEqual(expected_result, end_value)

    @patch('src.config.scot_div')
    def test_no_asterik_in_text(self, mock_div):
        """
        Used incase we find a paragraph refering to an asteriks thing that isnt referenced in the text
        we pull
        """
        # define fake content
        div = "fake_div"
        date = "mocked date"
        data = "mocked data"
        other_para = ["para info", "other_para"]

        # what is expected?
        expected_result = f"{date}\n\n\n\n{data}"

        bullet_points = f"<ul><li><span>{data}</span></li></ul>"


        # mock div value and create fake, incorrect,  html
        mock_div.return_value = div
        content = DummyHTML(date="<h2>" + date + "</h2>", data_list=bullet_points, div=div)
        content.add_para("*", other_para[0])
        content.add_para("**", other_para[1])
        html = content.as_string()

        # call function
        end_value = self.scraper.get_scotland_info(html)
        print(f"Received {end_value}")
        self.assertEqual(end_value, expected_result)

    @patch('src.config.scot_div')
    def test_get_asterik_twice(self, mock_div):
        """
        Check pulls first version of each paragraph
        """
        # define fake content
        div = "fake_div"
        date = "mocked date"
        data = "mocked* data**"
        other_para = ["para info", "other_para"]

        # what is expected?
        expected_result = f"{date}\n\n\n\n{data} \n\n\n\n------\n\n\n\n* {other_para[0]}\n\n\n\n** {other_para[1]}"

        bullet_points = f"<ul><li><span>{data}</span></li></ul>"


        # mock div value and create fake, incorrect,  html
        mock_div.return_value = div
        content = DummyHTML(date="<h2>" + date + "</h2>", data_list=bullet_points, div=div)
        content.add_para("*", other_para[0])
        content.add_para("**", other_para[1])

        content.add_para("*", other_para[1])
        content.add_para("**", other_para[0])
        html = content.as_string()

        # call function
        end_value = self.scraper.get_scotland_info(html)
        print(f"Received {end_value}")
        self.assertEqual(end_value, expected_result)

class TestSimpleGet(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", (0,))
        self.scraper = bot.scraper

    # Web requests covid_tests
    @patch('requests.get')
    def test_simple_get_requests_exception(self, mock_requests_get):
        # raise exceptio
        self.scraper.logger = Mock()
        mock_requests_get.side_effect = RequestException
        val, code = self.scraper.simple_get("http://www.google.com")
        self.assertIsNone(val)
        mock_requests_get.assert_called_once()
        self.scraper.logger.warning.assert_called_once()

    @patch('requests.get')
    def test_simple_get_requests_valid(self, mock_closing):
        # valid dat
        self.scraper.logger = Mock()
        dummy = DummyResponse(stat=200, cont="abc", head={'Content-Type': "<html>"})
        mock_closing.return_value = dummy
        val, code = self.scraper.simple_get("dummy")
        self.assertIsNotNone(val)
        mock_closing.assert_called_once()
        self.scraper.logger.warning.assert_not_called()

    @patch('requests.get')
    def test_simple_get_requests_invalid_status(self, mock_closing):
        # None status cod
        self.scraper.logger = Mock()
        dummy = DummyResponse(stat=None, cont="abc", head={'Content-Type': "<html>"})
        mock_closing.return_value = dummy
        val, code = self.scraper.simple_get("dummy")
        self.assertIsNone(val)
        mock_closing.assert_called_once()
        self.scraper.logger.warning.assert_not_called()

    @patch('requests.get')
    def test_simple_get_requests_invalid_content(self, mock_closing):
        # None conten
        self.scraper.logger = Mock()
        dummy = DummyResponse(stat=200, cont=None, head={'Content-Type': "<html>"})
        mock_closing.return_value = dummy
        val, status_code = self.scraper.simple_get("dummy")
        self.assertIsNone(val)
        mock_closing.assert_called_once()
        self.scraper.logger.warning.assert_not_called()

    @patch('requests.get')
    def test_simple_get_requests_invalid_headers(self, mock_closing):
        # invalid header
        dummy = DummyResponse(stat=200, cont="abc", head={'Content-Type': "<tml>"})
        self.scraper.logger = Mock()
        mock_closing.return_value = dummy
        val, status_code = self.scraper.simple_get("dummy")
        self.assertIsNone(val)
        mock_closing.assert_called_once()
        mock_closing.reset_mock()
        # None header
        dummy = DummyResponse(stat=200, cont="abc", head=None)
        mock_closing.return_value = dummy
        val, status_code = self.scraper.simple_get("dummy")
        self.assertIsNone(val)
        mock_closing.assert_called_once()
        self.scraper.logger.warning.assert_not_called()


class TestLogger(TestCase):
    def setUp(self):
        bot = Bot(False, "1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG", (0,))
        bot.logger = Mock()

        self.bot = bot
        self.scraper = bot.scraper

    def test_text_log_text_private(self):
        # check message is logged in private chat
        dummy_update = DummyUpdater(message_text="test", chat_type="private")
        self.bot.text_log(dummy_update, None)
        self.bot.logger.log.assert_called_once()
        self.bot.logger.log.assert_called_with(
            level=35,
            msg="User name (-1) has just sent a message in chat Private Chat with name (-1) saying \"test\"."
        )

    def test_text_log_text_groupchat(self):
        # check message is logged in group chat
        dummy_update = DummyUpdater(message_text="test", chat_type="chat")
        self.bot.text_log(dummy_update, None)
        self.bot.logger.log.assert_called_once()

    @patch('src.telegram_bot.Bot.send_text')
    def test_text_log_text_trigger_once(self, mock_send_text):
        # check trigger is activated for expected message (also check still logged)
        dummy_update = DummyUpdater(message_text="fs", chat_type="private")
        self.bot.text_log(dummy_update, None)
        mock_send_text.assert_called_once()
        self.bot.logger.log.assert_called_once()
        mock_send_text.reset_mock()

    @patch('src.telegram_bot.Bot.send_text')
    def test_text_log_text_trigger_multiple(self, mock_send_text):
        # check trigger is only activated once for multiple triggers
        dummy_update = DummyUpdater(message_text="fs fuck already", chat_type="private")

        self.bot.text_log(dummy_update, None)
        self.assertEqual(mock_send_text.call_count, 1)
        self.bot.logger.log.assert_called_once()

    @patch('src.telegram_bot.Bot.send_text')
    def test_text_log_text_trigger_capital(self, mock_send_text):
        # check trigger is not case sensitive
        dummy_update = DummyUpdater(message_text="alReAdy", chat_type="private")
        self.bot.text_log(dummy_update, None)
        mock_send_text.assert_called_once()
        self.bot.logger.log.assert_called_once()


class DummyResponse:
    def __init__(self, stat, cont, head):
        self.status_code = stat
        self.content = cont
        self.headers = head


def create_dummy_data(date="date", cases="0", perc="0.01%", deaths="0", intens="0", hospit="0", tests="0",
                      test_perc="0.01", dose_1="1111", dose_2="2222", dose_3="3333", dash_19=True, remove=-1):
    if dash_19:
        cov = "COVID-19"
    else:
        cov = "covid"
    fake_gov_data = [f"Scottish numbers: {date}\n",
                     f"{cases} new cases of COVID-19 reported. \n",
                     f"{tests} new tests for COVID-19 that reported results " +
                     f"{test_perc}% of these were positive \n",
                     f"{deaths} new reported death(s) of people who have tested positive \n",
                     f"{intens} people were in intensive care yesterday with recently confirmed COVID-19 \n",
                     f"{hospit} people were in hospital yesterday with recently confirmed COVID-19 \n",
                     f"{dose_1} people have received their first dose of a {cov} vaccination, {dose_2}" +
                     f"have received their second dose, and {dose_3} have received a third dose or booster\n"]

    if not remove == -1:
        del fake_gov_data[remove]

    fake_gov_data_str = ""
    for val in fake_gov_data:
        fake_gov_data_str = fake_gov_data_str + val

    return fake_gov_data_str


def create_dummy_expected_values(date="date", cases="0", perc="No longer provided", deaths="0", intens="0", hospit="0", tests="0",
                                 test_perc="0.01", dose_1="1111", dose_2="2222", dose_3="3333"):
    return {'Date': date, 'Number of cases': cases, 'Percent Newly Positive': perc,
            'Deaths': deaths, 'People in Intensive Care': intens, 'People in Hospital': hospit,
            'Returned Tests': tests, 'Percent Positive Tests': test_perc,
            'First Vaccination Dose': dose_1, 'Second Vaccination Dose': dose_2, 'Third Vaccination Dose': dose_3}
