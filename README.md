# covid_bot

Pulls scottish government covid stats to find updated data and send to telegram users. Checks every minute. Has subscribe feature for multiple users.

### The env.env file should have 2 variables and is located in the top level:

Two arguments need to be provided

- BOT_TOKEN: Telegram bot token
- OWNER_ID: Telegram user id of owner

It should look like so:

BOT_TOKEN=1234567890:ABCDEFGHIJKLMNO_PQRSTUVWX-YZABCDEFG

OWNER_ID=1234567
